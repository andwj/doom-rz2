;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;


#private

const FLOORSPEED = FRACUNIT

type floor_type_e = s32

const fl_lowerFloor        = 0  ; lower floor to highest surrounding floor
const fl_lowerTurbo        = 1  ; lower floor to highest surrounding floor VERY FAST
const fl_lowerToLowest     = 2  ; lower floor to lowest surrounding floor
const fl_lowerAndChange    = 3  ; lower floor to lowest surrounding floor and change floorpic
const fl_raiseFloor        = 4  ; raise floor to lowest surrounding CEILING
const fl_raiseToNearest    = 5  ; raise floor to next highest surrounding floor
const fl_raiseNearestTurbo = 6  ; raise to next highest floor, turbo-speed
const fl_raiseToTexture    = 7  ; raise floor to shortest height texture around it
const fl_raise24           = 8
const fl_raise24Change     = 9
const fl_raiseFloorCrush   = 10
const fl_raiseFloor512     = 11
const fl_donutRaise        = 12

type stair_type_e = s32

const stair_build8  = 0  ; slowly build by 8
const stair_turbo16 = 1  ; quickly build by 16

type floor_t = struct {
	.thinker    thinker_t
	.sector    ^sector_t

	.type       floor_type_e
	.dest       fixed_t
	.speed      fixed_t

	.direction  s32
	.texture    s32
	.special    s16
	.crush      bool
}


;
; Move a floor to it's destination (up or down)
;
fun T_MoveFloor (F ^floor_t) {
	let sec = [F .sector]
	let t = leveltime & 7

	let res = T_MoveFloorPlane sec [F .speed] [F .dest] [F .crush] [F .direction]

	if t == 0 {
		S_StartSound [ref sec .soundorg] sfx_stnmov
	}

	if res == RES_pastdest {
		; handle texture changes

		if [F .direction] == 1 {
			if [F .type] == fl_donutRaise {
				[sec .special]  = [F .special]
				[sec .floorpic] = [F .texture]
			}
		} else if [F .direction] == -1 {
			if [F .type] == fl_lowerAndChange {
				[sec .special]  = [F .special]
				[sec .floorpic] = [F .texture]
			}
		}

		[sec .specialdata] = NULL

		P_RemoveThinker (cast ^thinker_t F)

		S_StartSound [ref sec .soundorg] sfx_pstop
	}
}

;
; Handle floor type.
; Donuts are eaten elsewhere.
;
fun EV_DoFloor (ld ^line_t, kind floor_type_e -> bool) {
	let did = FALSE

	let secnum = s32 -1

	loop {
		secnum = P_FindSectorFromLineTag ld secnum
		break if secnum < 0

		let sec = [ref sectors secnum]

		; already moving?  if so, keep going...
		if null? [sec .specialdata] {
			; new floor thinker
			let F = cast ^floor_t Z_Calloc floor_t.size PU_LEVEL

			[F .thinker .func] = THINK_floor

			; set default parameters
			[F .sector]  = sec
			[F .type]    = kind
			[F .speed]   = FLOORSPEED
			[F .crush]   = FALSE

			if kind <= fl_lowerAndChange {
				[F .direction] = -1
			} else {
				[F .direction] = +1
			}

			if kind == fl_lowerFloor {
				[F .dest] = P_FindHighestFloorSurrounding sec

			} else if kind == fl_lowerTurbo {
				[F .speed] = FLOORSPEED * 4
				[F .dest]  = P_FindHighestFloorSurrounding sec

				if [F .dest] != [sec .floorh] {
					[F .dest] = [F .dest] + (8 * FRACUNIT)
				}

			} else if kind == fl_lowerToLowest {
				[F .dest] = P_FindLowestFloorSurrounding sec

			} else if kind == fl_raiseFloor {
				[F .dest] = P_FindLowestCeilingSurrounding sec
				[F .dest] = min [F .dest] [sec .ceilh]

			} else if kind == fl_raiseFloorCrush {
				[F .dest]  = P_FindLowestCeilingSurrounding sec
				[F .dest]  = min [F .dest] [sec .ceilh]
				[F .dest]  = [F .dest] - (8 * FRACUNIT)
				[F .crush] = TRUE

			} else if kind == fl_raiseToNearest {
				[F .dest] = P_FindNextHighestFloor sec [sec .floorh]

			} else if kind == fl_raiseNearestTurbo {
				[F .speed] = FLOORSPEED * 4
				[F .dest]  = P_FindNextHighestFloor sec [sec .floorh]

			} else if kind == fl_raise24 {
				[F .dest] = [sec .floorh] + (24 * FRACUNIT)

			} else if kind == fl_raise24Change {
				[F .dest] = [sec .floorh] + (24 * FRACUNIT)

				[sec .floorpic] = [[ld .front] .floorpic]
				[sec .special]  = [[ld .front] .special]

			} else if kind == fl_raiseFloor512 {
				[F .dest] = [sec .floorh] + (512 * FRACUNIT)

			} else if kind == fl_raiseToTexture {
				let tex_h = P_FindMinimumLowerTexHeight sec secnum

				[F .dest] = [sec .floorh] + tex_h

			} else if kind == fl_lowerAndChange {
				[F .dest] = P_FindLowestFloorSurrounding sec

				let model = P_FindModelSectorForChange sec secnum [F .dest]

				if ref? model {
					[F .texture] = [model .floorpic]
					[F .special] = [model .special]
				} else {
					[F .texture] = [sec .floorpic]
					[F .special] = 0
				}
			}

			P_AddThinker (cast ^thinker_t F)
			[sec .specialdata] = F

			did = TRUE
		}
	}

	return did
}

fun P_FindMinimumLowerTexHeight (sec ^sector_t, secnum s32 -> fixed_t) {
	let tex_h = fixed_t FRAC_MAX

	let i = s32 0
	loop while i < [sec .linecount] {
		let ld = [[sec .lines] i]

		if ([ld .flags] & ML_TWOSIDED) != 0 {
			let side1 = [ref sides [ld .sidenum 0]]
			let side2 = [ref sides [ld .sidenum 1]]

			if [side1 .bottom] >= 0 {
				let h = R_TextureHeight [side1 .bottom]
				tex_h = min tex_h h
			}

			if [side2 .bottom] >= 0 {
				let h = R_TextureHeight [side2 .bottom]
				tex_h = min tex_h h
			}
		}

		i = i + 1
	}

	return tex_h
}

fun P_FindModelSectorForChange (sec ^sector_t, secnum s32, dest_h fixed_t -> ^sector_t) {
	let i = s32 0
	loop while i < [sec .linecount] {
		let ld = [[sec .lines] i]

		let other = getNextSector ld sec

		if ref? other {
			if [other .floorh] == dest_h {
				return other
			}
		}

		i = i + 1
	}

	return NULL
}

;
; Build a staircase!
;
fun EV_BuildStairs (ld ^line_t, kind stair_type_e -> bool) {
	let did = FALSE

	let speed = fixed_t (FLOORSPEED / 4)
	let rise = fixed_t (8 * FRACUNIT)

	if kind == stair_turbo16 {
		speed = FLOORSPEED * 4
		rise  = 16 * FRACUNIT
	}

	let secnum = s32 -1

	loop {
		secnum = P_FindSectorFromLineTag ld secnum
		break if secnum < 0

		let sec = [ref sectors secnum]

		; already moving?  if so, keep going...
		if null? [sec .specialdata] {
			let height = [sec .floorh] + rise

			P_BuildStairInSector sec height speed

			let texture = [sec .floorpic]

			; Find next sector to raise
			;   1  Find 2-sided line with same sector side[0]
			;   2. Other side is the next sector to raise
			;   3. Sector has the same floor texture

			loop {
				let found = FALSE

				let i = s32 0
				loop while i < [sec .linecount] {
					let ld2 = [[sec .lines] i]

					let front = [ld2 .front]
					let back  = [ld2 .back]

					let two_sided = ([ld2 .flags] & ML_TWOSIDED) != 0
					let match_front = front == sec
					let match_back = ref? back

					if match_back {
						match_back = [back .floorpic] == texture
					}

					if two_sided and match_front and match_back {
						; andrewj: a vanilla bug here, it bumps the height *before*
						; checking that the neighbor sector has a mover in it.
						height = height + rise

						if null? [back .specialdata] {
							found  = TRUE

							sec    = back
							secnum = s32 (sec - sectors)
							secnum = secnum / sector_t.size

							P_BuildStairInSector sec height speed

							; we have a new sector, stop iterating over old one
							break
						}
					}

					i = i + 1
				}

				break if not found
			}

			did = TRUE
		}
	}

	return did
}

fun P_BuildStairInSector (sec ^sector_t, dest fixed_t, speed fixed_t) {
	let F = cast ^floor_t Z_Calloc floor_t.size PU_LEVEL

	[F .thinker .func] = THINK_floor

	[F .sector]    = sec
	[F .type]      = fl_raiseFloor
	[F .dest]      = dest
	[F .speed]     = speed
	[F .direction] = +1
	[F .crush]     = TRUE

	P_AddThinker (cast ^thinker_t F)
	[sec .specialdata] = F
}

;
; Special Stuff that can not be categorized
;
fun EV_DoDonut (ld ^line_t -> bool) {
	let did = FALSE

	let secnum = s32 -1

	loop {
		secnum = P_FindSectorFromLineTag ld secnum
		break if secnum < 0

		let s1 = [ref sectors secnum]

		; already moving?  if so, keep going...
		if null? [s1 .specialdata] {
			did = TRUE

			; andrewj: added this, not sure it can really happen though
			if [s1 .linecount] == 0 {
				I_Print "EV_DoDonut: sector has no linedefs!\n"
				break
			}

			let s2 = getNextSector [[s1 .lines] 0] s1

			; Vanilla Doom does not check if the linedef is one sided.  The
			; game does not crash, but reads invalid memory and causes the
			; sector floor to move "down" to some unknown height.

			if null? s2 {
				I_Print "EV_DoDonut: model linedef has no second sidedef!\n"
				break
			}

			let i = s32 0
			loop while i < [s2 .linecount] {
				let ld3 = [[s2 .lines] i]
				let s3  = [ld3 .back]

				if s3 != s1 {
					let s3_floorh   = fixed_t 0
					let s3_floorpic = s32 1

					if ref? s3 {
						s3_floorh   = [s3 .floorh]
						s3_floorpic = [s3 .floorpic]
					} else {
						; e6y
						; s3 is NULL, so
						; s3->floorh is an int at 0000:0000
						; s3->floorpic is a short at 0000:0008
						; Trying to emulate (values above)

						s3_floorh   = 0
						s3_floorpic = 1
					}

					; spawn rising slime
					let raiser = P_DonutMover s2 +1 s3_floorh

					[raiser .type]    = fl_donutRaise
					[raiser .texture] = s3_floorpic
					[raiser .special] = 0

					; spawn lowering donut-hole
					let downer = P_DonutMover s1 -1 s3_floorh
					break
				}

				i = i + 1
			}
		}
	}

	return did
}

fun P_DonutMover (sec ^sector_t, direction s32, dest fixed_t -> ^floor_t) {
	let F = cast ^floor_t Z_Calloc floor_t.size PU_LEVEL

	[F .thinker .func] = THINK_floor

	[F .sector]    = sec
	[F .type]      = fl_lowerFloor
	[F .dest]      = dest
	[F .speed]     = FLOORSPEED / 2
	[F .direction] = direction
	[F .crush]     = FALSE

	P_AddThinker (cast ^thinker_t F)
	[sec .specialdata] = F

	return F
}

;
; Move a floor plane and check for crushing
;
fun T_MoveFloorPlane (sec ^sector_t, speed fixed_t, dest fixed_t, crush bool, direction s32 -> result_e) {
	if direction == -1 {
		; DOWN
		let last  = [sec .floorh]
		let new_h = last - speed

		if new_h < dest {
			[sec .floorh] = dest

			let nofit = P_ChangeSector sec crush

			if nofit {
				[sec .floorh] = last
				P_ChangeSector sec crush
			}
			return RES_pastdest
		}

		[sec .floorh] = new_h

		let nofit = P_ChangeSector sec crush

		if nofit {
			[sec .floorh] = last
			P_ChangeSector sec crush

			return RES_crushed
		}

	} else if direction == 1 {
		; UP
		let last  = [sec .floorh]
		let new_h = last + speed

		if new_h > dest {
			[sec .floorh] = dest

			let nofit = P_ChangeSector sec crush

			if nofit {
				[sec .floorh] = last
				P_ChangeSector sec crush
			}
			return RES_pastdest
		}

		; COULD GET CRUSHED
		[sec .floorh] = new_h

		let nofit = P_ChangeSector sec crush

		if nofit {
			if not crush {
				[sec .floorh] = last
				P_ChangeSector sec crush
			}
			return RES_crushed
		}
	}

	return RES_ok
}
