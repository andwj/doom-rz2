;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;


; these are in engine module


#public

zero-var menuactive    bool
zero-var inhelpscreens bool

;
; configuration values
;
var mouseSensitivity s32 = 5

; Show messages has default, 0 = off, 1 = on
var showMessages s32 = 1

; Blocky mode, has default, 0 = high, 1 = normal
var detailLevel  s32 = 0
var screenblocks s32 = 10

#private

const LINEHEIGHT  = 16
const SKULLXOFF   = -32
const SKULLYOFF   = -5

; temp for screenblocks (0-8)
zero-var screenSize s32

type message_routine_t = fun (key s32)

type MenuMessageState = struct {
	.str    ^uchar  ; message to be printed
	.x       s32    ; draw location
	.y       s32    ;
	.input   bool   ; message requires a key press?
	.active  bool   ; a menu was active for the message?
	.func   ^message_routine_t
}

zero-var mess MenuMessageState

const MSG_SIZE = 100

type MenuSaveState = struct {
	.slot    s32   ; which slot to save in
	.along   s32   ; which char we are editing
	.enter   bool  ; are we entering a save description?

	.quick_slot s32  ; -1 = no quicksave slot picked!
	.quick_msg  [MSG_SIZE]uchar  ; used for quick load/save

	.strings [10][SAVESTRING_SIZE]uchar
	.oldstr      [SAVESTRING_SIZE]uchar  ; old description before edit
}

zero-var msave MenuSaveState

const PICK_SLOT_NOW = -2

const NUM_QUITMESSAGES = 8

rom-var endmessages [NUM_QUITMESSAGES]^uchar = {
	"are you sure you want to\nquit this great game?"
	"you want to quit?\nthen, thou hast lost an eighth!"
	"please don't leave, there's more\ndemons to toast!"
	"let's beat it -- this is turning\ninto a bloodbath!"

	"don't leave yet -- there's a\ndemon around that corner!"
	"if i were your boss, i'd \n deathmatch ya in a minute!"
	"look, bud. you leave now\nand you forfeit your body count!"
	"just leave. when you come\nback, i'll be waiting with a bat."
}

const ENDSTR_SIZE = 200
zero-var endstring [ENDSTR_SIZE]uchar

rom-var quitsounds1 [8]sfxenum_t = {
	sfx_pldeth sfx_dmpain sfx_popain sfx_slop
	sfx_telept sfx_posit1 sfx_posit3 sfx_sgtatk
}

rom-var quitsounds2 [8]sfxenum_t = {
	sfx_vilact sfx_getpow sfx_boscub sfx_slop
	sfx_skeswg sfx_kntdth sfx_bspact sfx_sgtatk
}

; strings...
const PRESSKEY   = "press a key."
const PRESSYN    = "press y or n."
const QUITMSG    = "are you sure you want to\nquit this great game?"
const LOADNET    = "you can't do load while in a net game!\n\n" + PRESSKEY
const QSAVEMSG   = "quicksave over your game?\n\n" + PRESSYN
const QSAVESPOT  = "you haven't picked a quicksave slot yet!\n\n" + PRESSKEY
const QLOADMSG   = "do you want to quickload the game?\n\n" + PRESSYN
const QLOADNET   = "you can't quickload during a netgame!\n\n" + PRESSKEY
const SAVEDEAD   = "you can't save if you aren't playing!\n\n" + PRESSKEY
const QSPROMPT   = "quicksave over your game named\n\n'%s'?\n\n" + PRESSYN
const QLPROMPT   = "do you want to quickload the game named\n\n'%s'?\n\n" + PRESSYN
const NEWGAME    = "you can't start a new game\nwhile in a network game.\n\n" + PRESSKEY
const NIGHTMARE  = "are you sure? this skill level\nisn't even remotely fair.\n\n" + PRESSYN
const SWSTRING   = "this is the shareware version of doom.\n\nyou need to order the entire trilogy.\n\n" + PRESSKEY

const MSGOFF    = "Messages OFF"
const MSGON     = "Messages ON"
const NETEND    = "you can't end a netgame!\n\n" + PRESSKEY
const ENDGAME   = "are you sure you want to end the game?\n\n" + PRESSYN
const DOSY      = "(press y to quit to dos.)"
const DETAILHI  = "High detail"
const DETAILLO  = "Low detail"

const EMPTYSTRING    = "-- free slot --"
const INVALIDSTRING  = "-- incompatible file --"

const GAMMALVL0 = "Gamma correction OFF"
const GAMMALVL1 = "Gamma correction level 1"
const GAMMALVL2 = "Gamma correction level 2"
const GAMMALVL3 = "Gamma correction level 3"
const GAMMALVL4 = "Gamma correction level 4"

rom-var gammamsg [5]^uchar = {
	GAMMALVL0
	GAMMALVL1
	GAMMALVL2
	GAMMALVL3
	GAMMALVL4
}

;;
;; MENU TYPES
;;

type menu_routine_t = fun (pos s32, arrow s32)
type draw_routine_t = fun ()

type menu_t = struct {
	.x        s32
	.y        s32

	.parent  ^menu_t
	.drawer  ^draw_routine_t

	.items   ^[0]menuitem_t
	.total    s32  ; # of menu items
	.last     s32  ; last item user was on
}

type menuitem_t = struct {
	.pic   ^uchar
	.func  ^menu_routine_t  ; called when activated by user

	.ascii   s32  ; hotkey in menu
	.status  s32  ; one of the MST_XXX values below
}

const MST_BLANK  = -1  ; a blank line, never visitable
const MST_INERT  = 0   ; an unusable line
const MST_NORMAL = 1   ; a normal, usable line
const MST_ARROWS = 2   ; a slider (etc), uses arrow keys

; current menudef
zero-var currentMenu ^menu_t

zero-var itemOn     s32  ; menu item the skull is on
zero-var skullAnim  s32  ; skull animation state
zero-var chosenEpi  s32  ; the episode to play

; graphic name of skulls, etc
rom-var skullPics  [2]^uchar = {"M_SKULL1" "M_SKULL2"}
rom-var detailPics [2]^uchar = {"M_GDHIGH" "M_GDLOW"}
rom-var msgPics    [2]^uchar = {"M_MSGOFF" "M_MSGON"}

;;
;; DOOM MENU
;;

var MainMenuItems [6]menuitem_t = {
	{ "M_NGAME"  M_NewGame  'N' MST_NORMAL }
	{ "M_OPTION" M_Options  'O' MST_NORMAL }
	{ "M_LOADG"  M_LoadGame 'L' MST_NORMAL }
	{ "M_SAVEG"  M_SaveGame 'S' MST_NORMAL }
	{ "M_RDTHIS" M_ReadThis 'R' MST_NORMAL }
	{ "M_QUITG"  M_QuitDOOM 'Q' MST_NORMAL }
}

var MainMenu menu_t = {
	.x 97 .y 64
	.parent NULL
	.drawer M_DrawMainMenu
	.items  MainMenuItems
	.total  6
	.last   0
}

;;
;; EPISODE SELECT
;;

var EpisodeMenuItems [4]menuitem_t = {
	{ "M_EPI1" M_Episode 'K' MST_NORMAL }
	{ "M_EPI2" M_Episode 'T' MST_NORMAL }
	{ "M_EPI3" M_Episode 'I' MST_NORMAL }
	{ "M_EPI4" M_Episode 'T' MST_NORMAL }
}

var EpiMenu menu_t = {
	.x 48 .y 63
	.parent MainMenu
	.drawer M_DrawEpisode
	.items  EpisodeMenuItems
	.total  4
	.last   0
}

;;
;; NEW GAME
;;

var NewGameItems [5]menuitem_t = {
	{ "M_JKILL"  M_ChooseSkill 'I' MST_NORMAL }
	{ "M_ROUGH"  M_ChooseSkill 'R' MST_NORMAL }
	{ "M_HURT"   M_ChooseSkill 'H' MST_NORMAL }
	{ "M_ULTRA"  M_ChooseSkill 'U' MST_NORMAL }
	{ "M_NMARE"  M_ChooseSkill 'N' MST_NORMAL }
}

var NewMenu menu_t = {
	.x 48 .y 63
	.parent EpiMenu
	.drawer M_DrawNewGame
	.items  NewGameItems
	.total  5
	.last   2  ; Hurt Me Plenty
}

;;
;; OPTIONS MENU
;;

var OptionsMenuItems [8]menuitem_t = {
	{ "M_ENDGAM" M_EndGame        'E' MST_NORMAL }
	{ "M_MESSG"  M_ChangeMessages 'M' MST_NORMAL }
	{ "M_DETAIL" M_ChangeDetail   'G' MST_NORMAL }
	{ "M_SCRNSZ" M_SizeDisplay    'S' MST_ARROWS }
	{ ""         NULL              0  MST_BLANK  }
	{ "M_MSENS"  M_ChangeSens     'M' MST_ARROWS }
	{ ""         NULL              0  MST_BLANK  }
	{ "M_SVOL"   M_Sound          'S' MST_NORMAL }
}

var OptionsMenu menu_t = {
	.x 60 .y 37
	.parent MainMenu
	.drawer M_DrawOptions
	.items  OptionsMenuItems
	.total  8
	.last   0
}

;;
;; READ THIS!
;;

var ReadMenu1Items [1]menuitem_t = {
	{ "" M_ReadThis2 0 MST_NORMAL }
}

var ReadMenu2Items [1]menuitem_t = {
	{ "" M_FinishReadThis 0 MST_NORMAL }
}

var ReadMenu1 menu_t = {
	.x 280 .y 185
	.parent MainMenu
	.drawer M_DrawReadThis1
	.items  ReadMenu1Items
	.total  1
	.last   0
}

var ReadMenu2 menu_t = {
	.x 330 .y 175
	.parent ReadMenu1
	.drawer M_DrawReadThis2
	.items  ReadMenu2Items
	.total  1
	.last   0
}

;;
;; SOUND VOLUME MENU
;;

var SoundMenuItems [4]menuitem_t = {
	{ "M_SFXVOL" M_SfxVol   'S' MST_ARROWS }
	{ ""         NULL        0  MST_BLANK  }
	{ "M_MUSVOL" M_MusicVol 'M' MST_ARROWS }
	{ ""         NULL        0  MST_BLANK  }
}

var SoundMenu menu_t = {
	.x 80 .y 64
	.parent OptionsMenu
	.drawer M_DrawSound
	.items  SoundMenuItems
	.total  4
	.last   0
}

;;
;; LOAD GAME MENU
;;

var LoadMenuItems [6]menuitem_t = {
	{ "" M_LoadSelect '1' MST_NORMAL }
	{ "" M_LoadSelect '2' MST_NORMAL }
	{ "" M_LoadSelect '3' MST_NORMAL }
	{ "" M_LoadSelect '4' MST_NORMAL }
	{ "" M_LoadSelect '5' MST_NORMAL }
	{ "" M_LoadSelect '6' MST_NORMAL }
}

var LoadMenu menu_t = {
	.x 80 .y 54
	.parent MainMenu
	.drawer M_DrawLoad
	.items  LoadMenuItems
	.total  6
	.last   0
}

;;
;; SAVE GAME MENU
;;

var SaveMenuItems [6]menuitem_t = {
	{ "" M_SaveSelect '1' MST_NORMAL }
	{ "" M_SaveSelect '2' MST_NORMAL }
	{ "" M_SaveSelect '3' MST_NORMAL }
	{ "" M_SaveSelect '4' MST_NORMAL }
	{ "" M_SaveSelect '5' MST_NORMAL }
	{ "" M_SaveSelect '6' MST_NORMAL }
}

var SaveMenu menu_t = {
	.x 80 .y 54
	.parent MainMenu
	.drawer M_DrawSave
	.items  SaveMenuItems
	.total  6
	.last   0
}

;------------------------------------------------------------------------

;
; M_ReadSaveStrings
; read the descriptions from the savegame files.
;
fun M_ReadSaveStrings () {
	let i = s32 0
	loop while i < 6 {
		let desc = [ref msave .strings i]

		let status = P_ReadSaveDescription i desc

		if status == 0 {
			M_StringCopy desc EMPTYSTRING SAVESTRING_SIZE
		} else if status < 0 {
			M_StringCopy desc INVALIDSTRING SAVESTRING_SIZE
		}

		if status > 0 {
			[LoadMenuItems i .status] = MST_NORMAL
		} else {
			[LoadMenuItems i .status] = MST_INERT
		}

		i = i + 1
	}
}

;
; M_LoadGame & Cie.
;
fun M_DrawLoad () {
	V_DrawPatchName 72 28 "M_LOADG"

	let i = s32 0
	loop while i < 6 {
		let y = i * LINEHEIGHT
		y = y + [LoadMenu .y]
		M_DrawSaveLoadBorder [LoadMenu .x] y

		let desc = cast ^uchar [ref msave .strings i]
		HU_DrawText [LoadMenu .x] y desc

		i = i + 1
	}
}

;
; Draw border for a single savegame description
;
fun M_DrawSaveLoadBorder (x s32, y s32) {
	x = x - 8
	y = y + 7

	V_DrawPatchName x y "M_LSLEFT"

	let i = s32 24
	loop until i == 0 {
		i = i - 1
		x = x + 8
		V_DrawPatchName x y "M_LSCNTR"
	}

	x = x + 8
	V_DrawPatchName x y "M_LSRGHT"
}

;
; User wants to load this game
;
fun M_LoadSelect (pos s32, arrow s32) {
	M_ClearMenus
	G_LoadGame pos
}

;
; Selected from DOOM menu
;
fun M_LoadGame (pos s32, arrow s32) {
	if netgame {
		M_StartMessage LOADNET NULL FALSE
		return
	}

	M_SetupNextMenu LoadMenu
	M_ReadSaveStrings
}

;
;  M_SaveGame & Cie.
;
fun M_DrawSave () {
	V_DrawPatchName 72 28 "M_SAVEG"

	let i = s32 0
	loop while i < 6 {
		let y = i * LINEHEIGHT
		y = y + [SaveMenu .y]
		M_DrawSaveLoadBorder [SaveMenu .x] y

		let desc = cast ^uchar [ref msave .strings i]
		HU_DrawText [SaveMenu .x] y desc

		; show an underscore at end of line being edited
		if [msave .enter] {
			if i == [msave .slot] {
				let width = HU_TextWidth desc
				let x = [SaveMenu .x] + width
				HU_DrawText x y "_"
			}
		}

		i = i + 1
	}
}

;
; M_Responder calls this when user is finished
;
fun M_DoSave (slot s32) {
	M_ClearMenus
	G_SaveGame slot [ref msave .strings slot]

	; picked quicksave slot yet?
	if [msave .quick_slot] == PICK_SLOT_NOW {
		[msave .quick_slot] = slot
	}
}

;
; User wants to save. Start string input for M_Responder
;
fun M_SaveSelect (pos s32, arrow s32) {
	[msave .slot] = pos

	; we are going to be intercepting all chars
	[msave .enter] = TRUE

	; need to turn on text input in system code
	let x1 = [LoadMenu .x] - 11
	let x2 = x1 + (26 * 8)

	let y1 = pos * LINEHEIGHT
	y1 = [LoadMenu .y] + y1 - 4
	let y2 = y1 + (LINEHEIGHT - 2)

	I_StartTextInput x1 y1 x2 y2

	; save old string in case the user cancels
	let desc = [ref msave .strings pos]

	M_StringCopy [ref msave .oldstr] desc SAVESTRING_SIZE

	; clear string when it is just showing an empty slot
	let is_empty   = M_StrCmp desc EMPTYSTRING   == 0
	let is_invalid = M_StrCmp desc INVALIDSTRING == 0

	if is_empty or is_invalid {
		[desc 0] = 0
	}

	[msave .along] = s32 M_StrLen desc
}

;
; Selected from DOOM menu
;
fun M_SaveGame (pos s32, arrow s32) {
	if not usergame {
		M_StartMessage SAVEDEAD NULL FALSE
		return
	}

	if gamestate != GS_LEVEL {
		return
	}

	M_ReadSaveStrings
	M_SetupNextMenu SaveMenu
}

;
; M_QuickSave
;
fun M_QuickSaveResponse (key s32) {
	if key == key_menu_confirm {
		M_DoSave [msave .quick_slot]
	}
}

fun M_QuickSave () {
	if not usergame {
		S_StartSound NULL sfx_oof
		return
	}

	if gamestate != GS_LEVEL {
		return
	}

	if [msave .quick_slot] < 0 {
		M_ReadSaveStrings
		M_StartControlPanel
		M_SetupNextMenu SaveMenu
		[msave .quick_slot] = PICK_SLOT_NOW
		return
	}

	let msg = [ref msave .quick_msg]

	M_StringCopy   msg QSAVEMSG MSG_SIZE
	M_StartMessage msg M_QuickSaveResponse TRUE
}

;
; M_QuickLoad
;
fun M_QuickLoadResponse (key s32) {
	if key == key_menu_confirm {
		M_LoadSelect [msave .quick_slot] 0
	}
}

fun M_QuickLoad () {
	if netgame {
		M_StartMessage QLOADNET NULL FALSE
		return
	}

	if [msave .quick_slot] < 0 {
		M_StartMessage QSAVESPOT NULL FALSE
		return
	}

	let msg = [ref msave .quick_msg]

	M_StringCopy   msg QLOADMSG MSG_SIZE
	M_StartMessage msg M_QuickLoadResponse TRUE
}

fun M_SaveGameInput (ev ^event_t) {
	let key = [ev .data1]

	if key == KEY_BACKSPACE {
		[msave .along] = [msave .along] - 1
		[msave .along] = max [msave .along] 0

		let desc = [ref msave .strings [msave .slot]]

		; keep the string NUL-terminated
		[desc [msave .along]] = 0
		return
	}

	if key == KEY_ESCAPE {
		[msave .enter] = FALSE
		I_StopTextInput

		; restore the original string
		let desc = [ref msave .strings [msave .slot]]
		M_StringCopy desc [ref msave .oldstr]  SAVESTRING_SIZE
		return
	}

	if key == KEY_ENTER {
		[msave .enter] = FALSE
		I_StopTextInput

		; require a non-empty description.  if empty, cancel the save
		if [msave .along] > 0 {
			M_DoSave [msave .slot]
		}

		return
	}

	; andrewj: simplified this logic, prefer using the fully translated
	;          key in the `data3` field, but fallback to `data2` when it
	;          is zero.

	if [ev .data3] != 0 {
		key = [ev .data3]
	} else if [ev .data2] != 0 {
		key = [ev .data2]
	}

	if key >= 'a' and key <= 'z' {
		key = key - 32
	}

	; ignore characters not in the font
	if key == ' ' {
		; ok
	} else if key < HU_FONTSTART {
		return
	} else if key > HU_FONTEND {
		return
	}

	; out of room?
	if [msave .along] >= (SAVESTRING_SIZE - 1) {
		return
	}

	; add the character to the end
	let desc = [ref msave .strings [msave .slot]]

	[desc [msave .along]] = uchar key
	[msave .along] = [msave .along] + 1

	; keep the string NUL-terminated
	[desc [msave .along]] = 0
}

;------------------------------------------------------------------------

;
; Read This Menus
;
fun M_DrawReadThis1 () {
	inhelpscreens = TRUE

	V_DrawPatchName 0 0 "HELP2"
}

;
; Read This Menus - optional second page.
;
; We only ever draw the second page if gameversion == exe_doom_1_9
; and gamemode == registered.
;
fun M_DrawReadThis2 () {
	inhelpscreens = TRUE

	V_DrawPatchName 0 0 "HELP1"
}

fun M_DrawReadThisCommercial () {
	inhelpscreens = TRUE

	V_DrawPatchName 0 0 "HELP"
}

;
; M_ReadThis
;
fun M_ReadThis (pos s32, arrow s32) {
	M_SetupNextMenu ReadMenu1
}

fun M_ReadThis2 (pos s32, arrow s32) {
	M_SetupNextMenu ReadMenu2
}

fun M_FinishReadThis (pos s32, arrow s32) {
	M_SetupNextMenu MainMenu
}

;
; M_DrawMainMenu
;
fun M_DrawMainMenu () {
	V_DrawPatchName 94 2 "M_DOOM"
}

;
; M_NewGame
;
fun M_DrawNewGame () {
	V_DrawPatchName 96 14 "M_NEWG"
	V_DrawPatchName 54 38 "M_SKILL"
}

fun M_NewGame (pos s32, arrow s32) {
	if netgame {
		if not demoplayback {
			M_StartMessage NEWGAME NULL FALSE
			return
		}
	}

	; Chex Quest disabled the episode select screen, as did Doom II.
	if gameversion == exe_chex {
		chosenEpi = 1
		M_SetupNextMenu NewMenu
	} else if gamemode == commercial {
		M_SetupNextMenu NewMenu
	} else {
		M_SetupNextMenu EpiMenu
	}
}

;
; M_Episode
;
fun M_DrawEpisode () {
	V_DrawPatchName 54 38 "M_EPISOD"
}

fun M_VerifyNightmare (key s32) {
	if key == key_menu_confirm {
		M_ClearMenus
		G_DeferedInitNew sk_nightmare chosenEpi 1
	}
}

fun M_ChooseSkill (pos s32, arrow s32) {
	if pos == sk_nightmare {
		M_StartMessage NIGHTMARE M_VerifyNightmare TRUE
		return
	}

	let skill = skill_t pos

	M_ClearMenus
	G_DeferedInitNew skill chosenEpi 1
}

fun M_Episode (pos s32, arrow s32) {
	if gamemode == shareware {
		; any episode except the first shows a message
		if pos != 0 {
			M_StartMessage  SWSTRING NULL FALSE
			M_SetupNextMenu ReadMenu1
			return
		}
	}

	chosenEpi = pos + 1
	M_SetupNextMenu NewMenu
}

;
; M_EndGame
;
fun M_EndGameResponse (key s32) {
	if key == key_menu_confirm {
		[currentMenu .last] = itemOn

		M_ClearMenus
		D_StartTitle
	}
}

fun M_EndGame (pos s32, arrow s32) {
	if not usergame {
		S_StartSound NULL sfx_oof
		return
	}

	if netgame {
		M_StartMessage NETEND NULL FALSE
		return
	}

	M_StartMessage ENDGAME M_EndGameResponse TRUE
}

;
; M_QuitDOOM
;
fun M_QuitResponse (key s32) {
	if key == key_menu_confirm {
		if not netgame {
			M_PlayQuitSound
			I_WaitVBL 105
		}

		I_Quit
	}
}

fun M_PlayQuitSound () {
	let sfx = sfxenum_t 0

	let r = M_Random
	r = r + skullAnim
	r = r & 7

	if gamemode == commercial {
		S_StartSound NULL [quitsounds2 r]
	} else {
		S_StartSound NULL [quitsounds1 r]
	}
}

fun M_SelectEndMessage (-> ^uchar) {
	let r = M_Random
	r = r + skullAnim
	r = r % NUM_QUITMESSAGES

	return [endmessages r]
}

fun M_QuitDOOM (pos s32, arrow s32) {
	let msg = M_SelectEndMessage

	M_StringCopy   endstring msg             ENDSTR_SIZE
	M_StringConcat endstring ("\n\n" + DOSY) ENDSTR_SIZE

	M_StartMessage endstring M_QuitResponse TRUE
}

;------------------------------------------------------------------------

;
; Change Sfx & Music volumes
;
fun M_DrawSound () {
	V_DrawPatchName 60 38 "M_SVOL"

	let y = [SoundMenu .y] + (1 * LINEHEIGHT)
	M_DrawThermo [SoundMenu .x] y 16 sfxVolume

	y = [SoundMenu .y] + (3 * LINEHEIGHT)
	M_DrawThermo [SoundMenu .x] y 16 musicVolume
}

fun M_Sound (pos s32, arrow s32) {
	M_SetupNextMenu SoundMenu
}

fun M_SfxVol (pos s32, arrow s32) {
	if arrow < 0 {
		sfxVolume = sfxVolume - 1
		sfxVolume = max sfxVolume 0

	} else if arrow > 0 {
		sfxVolume = sfxVolume + 1
		sfxVolume = min sfxVolume 15
	}

	let vol = sfxVolume * 8

	S_SetSfxVolume vol
}

fun M_MusicVol (pos s32, arrow s32) {
	if arrow < 0 {
		musicVolume = musicVolume - 1
		musicVolume = max musicVolume 0

	} else if arrow > 0 {
		musicVolume = musicVolume + 1
		musicVolume = min musicVolume 15
	}

	let vol = musicVolume * 8

	S_SetMusicVolume vol
}

;
; M_Options
;
fun M_DrawOptions () {
	V_DrawPatchName 108 15 "M_OPTTTL"

	let msg_pic    = [msgPics    showMessages]
	let detail_pic = [detailPics detailLevel]

	let x = [OptionsMenu .x] + 120
	let y = [OptionsMenu .y] + (LINEHEIGHT * 1)

	V_DrawPatchName x y msg_pic

	x = [OptionsMenu .x] + 175
	y = [OptionsMenu .y] + (LINEHEIGHT * 2)

	V_DrawPatchName x y detail_pic

	x = [OptionsMenu .x]
	y = [OptionsMenu .y] + (LINEHEIGHT * 4)

	M_DrawThermo x y  9 screenSize

	y = [OptionsMenu .y] + (LINEHEIGHT * 6)

	M_DrawThermo x y 10 mouseSensitivity
}

fun M_Options (pos s32, arrow s32) {
	M_SetupNextMenu OptionsMenu
}

fun M_ChangeSens (pos s32, arrow s32) {
	if arrow < 0 {
		mouseSensitivity = mouseSensitivity - 1
		mouseSensitivity = max mouseSensitivity 0

	} else if arrow > 0 {
		mouseSensitivity = mouseSensitivity + 1
		mouseSensitivity = min mouseSensitivity 9
	}
}

fun M_ChangeDetail (pos s32, arrow s32) {
	; andrewj: removed support for low detail mode
}

fun M_SizeDisplay (pos s32, arrow s32) {
	if arrow < 0 {
		screenSize = screenSize - 1
		screenSize = max screenSize 0

	} else if arrow > 0 {
		screenSize = screenSize + 1
		screenSize = min screenSize 8
	}

	screenblocks = screenSize + 3

	R_SetViewSize screenblocks detailLevel
}

;
; Toggle messages on/off
;
fun M_ChangeMessages (pos s32, arrow s32) {
	showMessages = s32 (showMessages == 0)

	if showMessages > 0 {
		D_ConsoleMessage MSGON
	} else {
		D_ConsoleMessage MSGOFF
	}

	; tell hud code to force display of this message
	message_force = TRUE
}

;
; Misc Functions
;
fun M_DrawThermo (x s32, y s32, steps s32, value s32) {
	value = value * 8
	let dot_x = x + 8 + value

	V_DrawPatchName x y "M_THERML"

	loop until steps == 0 {
		steps = steps - 1
		x = x + 8
		V_DrawPatchName x y "M_THERMM"
	}

	x = x + 8
	V_DrawPatchName x y "M_THERMR"

	V_DrawPatchName dot_x y "M_THERMO"
}

;------------------------------------------------------------------------

fun M_StartMessage (str ^uchar, func ^message_routine_t, input bool) {
	[mess .str]    = str
	[mess .func]   = func
	[mess .input]  = input
	[mess .active] = menuactive

	menuactive = TRUE
}

fun M_StopMessage () {
	menuactive = [mess .active]
	[mess .str]  = NULL
}

fun M_DrawMessage () {
	; Horiz. & Vertically center string and print it.

	let y = HU_TextHeight [mess .str]
	y = (SCREENHEIGHT - y) / 2

	let line = stack-var [MSG_SIZE]uchar

	let s = cast ^uchar [mess .str]

	loop until [s] == 0 {
		; copy a single line into the buffer, up to next LF or NUL
		let len = s32 0

		loop {
			let ch = [s]
			break if ch == 0

			s = s + 1
			break if ch == '\u000A'

			; avoid overflowing buffer, truncate the line
			if len < (MSG_SIZE - 2) {
				[line len] = ch
				len = len + 1
			}
		}

		[line len] = 0

		let x = HU_TextWidth line
		x = (SCREENWIDTH - x) / 2

		HU_DrawText x y line

		y = y + 10
	}
}

;
; M_ClearMenus
;
fun M_ClearMenus () {
	menuactive = FALSE
}

;
; M_SetupNextMenu
;
fun M_SetupNextMenu (def ^menu_t) {
	currentMenu = def
	itemOn      = [def .last]
}

fun M_FindShortcut (ascii s32 -> s32) {
	if ascii >= 'a' and ascii <= 'z' {
		ascii = ascii - 32
	}

	let menu  = currentMenu

	let start = itemOn
	let cur   = itemOn

	loop {
		cur = cur + 1
		cur = cur % [menu .total]
		break if cur == start

		let item = [ref [menu .items] cur]

		if ascii == [item .ascii] {
			return cur
		}
	}

	return -1  ; not found
}

fun M_BumpGamma () {
	usegamma = usegamma + 1
	usegamma = usegamma % 5

	D_ConsoleMessage [gammamsg usegamma]
	V_PaletteNormal
}

;------------------------------------------------------------------------

#public

;;
;; CONTROL PANEL
;;

;
; M_Responder
;
fun M_Responder (ev ^event_t -> bool) {
	; WISH: support mouse by drawing a pointer, moving that pointer
	;       around, and allowing clicks on menu items.

	; "close" button pressed on window?
	if [ev .type] == ev_quit {
		; andrewj: changed this to quit immediately instead of asking,
		;          as that seems more in line with user expectations.
		I_Quit

		return TRUE
	}

	if [ev .type] != ev_keydown {
		return FALSE
	}

	; save game string input?
	if [msave .enter] {
		M_SaveGameInput ev
		return TRUE
	}

	let key = [ev .data1]
	if key == 0 {
		return FALSE
	}

	; make a screenshot?
	if key == key_menu_screenshot {
		G_ScreenShot
		return TRUE
	}

	; take care of any messages that need input
	if ref? [mess .str] {
		if [mess .input] {
			if not matches? key ' ' KEY_ESCAPE key_menu_confirm key_menu_abort {
				return FALSE
			}
		}

		M_StopMessage
		M_ClearMenus

		if ref? [mess .func] {
			call [mess .func] key
		}

		S_StartSound NULL sfx_swtchx
		return TRUE
	}

	if not menuactive {
		; pop-up the menu?
		if key == key_menu_activate {
			M_StartControlPanel
			S_StartSound NULL sfx_swtchn
			return TRUE
		}

		; -- F-Keys --

		; Screen size down
		if key == key_menu_decscreen {
			if automapactive or chat_on {
				return FALSE
			}
			M_SizeDisplay 0 -1
			S_StartSound NULL sfx_stnmov
			return TRUE
		}

		; Screen size up
		if key == key_menu_incscreen {
			if automapactive or chat_on {
				return FALSE
			}
			M_SizeDisplay 0 +1
			S_StartSound NULL sfx_stnmov
			return TRUE
		}

		; Help key
		if key == key_menu_help {
			M_StartControlPanel

			if gameversion >= exe_ultimate {
				M_SetupNextMenu ReadMenu2
			} else {
				M_SetupNextMenu ReadMenu1
			}

			S_StartSound NULL sfx_swtchn
			return TRUE
		}

		; Save
		if key == key_menu_save {
			M_StartControlPanel
			M_SaveGame 0 0
			S_StartSound NULL sfx_swtchn
			return TRUE
		}

		; Load
		if key == key_menu_load {
			M_StartControlPanel
			M_LoadGame 0 0
			S_StartSound NULL sfx_swtchn
			return TRUE
		}

		; Sound volume
		if key == key_menu_volume {
			M_StartControlPanel
			M_SetupNextMenu SoundMenu
			S_StartSound NULL sfx_swtchn
			return TRUE
		}

		; Detail toggle
		if key == key_menu_detail {
			M_ChangeDetail 0 0
			S_StartSound NULL sfx_swtchn
			return TRUE
		}

		; Quicksave
		if key == key_menu_qsave {
			M_QuickSave
			S_StartSound NULL sfx_swtchn
			return TRUE
		}

		; Quickload
		if key == key_menu_qload {
			M_QuickLoad
			S_StartSound NULL sfx_swtchn
			return TRUE
		}

		; End game
		if key == key_menu_endgame {
			M_EndGame 0 0
			S_StartSound NULL sfx_swtchn
			return TRUE
		}

		; Toggle messages
		if key == key_menu_messages {
			M_ChangeMessages 0 0
			S_StartSound NULL sfx_swtchn
			return TRUE
		}

		; Quit DOOM
		if key == key_menu_quit {
			S_StartSound NULL sfx_swtchn
			M_QuitDOOM 0 0
			return TRUE
		}

		; Gamma toggle
		if key == key_menu_gamma {
			M_BumpGamma
			S_StartSound NULL sfx_stnmov
			return TRUE
		}

		return FALSE
	}

	; -- Keys usable within menu --

	let menu = currentMenu
	let item = [ref [menu .items] itemOn]

	; Deactivate menu
	if key == key_menu_activate {
		[menu .last] = itemOn
		M_ClearMenus
		S_StartSound NULL sfx_swtchx
		return TRUE
	}

	; Activate menu item
	if key == key_menu_forward {
		if ref? [item .func] {
			[menu .last] = itemOn

			if [item .status] == MST_NORMAL {
				call [item .func] itemOn 0
				S_StartSound NULL sfx_pistol

			} else if matches? [item .status] MST_NORMAL MST_ARROWS {
				call [item .func] itemOn +1  ; right arrow
				S_StartSound NULL sfx_stnmov
			}
		}

		return TRUE
	}

	; Go back to previous menu
	if key == key_menu_back {
		[menu .last] = itemOn

		if ref? [menu .parent] {
			M_SetupNextMenu [menu .parent]
			S_StartSound NULL sfx_swtchn
		}

		return TRUE
	}

	; Move down to next item
	if key == key_menu_down {
		loop {
			itemOn = itemOn + 1
			itemOn = itemOn % [menu .total]

			item = [ref [menu .items] itemOn]

			break if [item .status] != MST_BLANK
		}

		S_StartSound NULL sfx_pstop
		return TRUE
	}

	; Move back up to previous item
	if key == key_menu_up {
		loop {
			itemOn = itemOn + [menu .total] + -1
			itemOn = itemOn % [menu .total]

			item = [ref [menu .items] itemOn]

			break if [item .status] != MST_BLANK
		}

		S_StartSound NULL sfx_pstop
		return TRUE
	}

	; Slide slider left
	if key == key_menu_left {
		if [item .status] == MST_ARROWS {
			call [item .func] itemOn -1
			S_StartSound NULL sfx_stnmov
		}

		return TRUE
	}

	; Slide slider right
	if key == key_menu_right {
		if [item .status] == MST_ARROWS {
			call [item .func] itemOn +1
			S_StartSound NULL sfx_stnmov
		}

		return TRUE
	}

	; Keyboard shortcut?
	key = [ev .data2]
	if key != 0 {
		let pos = M_FindShortcut key

		if pos >= 0 {
			itemOn = pos
			S_StartSound NULL sfx_pstop
			return TRUE
		}
	}

	return FALSE
}

;
; M_StartControlPanel
;
fun M_StartControlPanel () {
	; intro might call this repeatedly
	if menuactive {
		return
	}

	M_SetupNextMenu MainMenu

	menuactive = TRUE
}

;
; M_Drawer
; Called after the view has been rendered,
; but before it has been blitted.
;
fun M_Drawer () {
	inhelpscreens = FALSE

	if ref? [mess .str] {
		M_DrawMessage
		return
	}

	if not menuactive {
		return
	}

	; draw menu...
	let menu = currentMenu

	if ref? [menu .drawer] {
		call [menu .drawer]
	}

	let x     = [menu .x]
	let y     = [menu .y]
	let total = [menu .total]

	let i = s32 0
	loop while i < total {
		let pic = [[menu .items] i .pic]

		if ref? pic {
			if [pic] != 0 {
				V_DrawPatchName x y pic
			}
		}

		y = y + LINEHEIGHT
		i = i + 1
	}

	; draw skull
	let frame = skullAnim & 8
	frame = frame >> 3

	x = x + SKULLXOFF
	y = itemOn * LINEHEIGHT
	y = y + [menu .y] + SKULLYOFF

	V_DrawPatchName x y [skullPics frame]
}

;
; M_Ticker
;
fun M_Ticker () {
	skullAnim = skullAnim + 1
}

;
; M_Init
;
fun M_Init () {
	menuactive = FALSE
	screenSize = screenblocks - 3

	[mess .str]    = NULL
	[mess .active] = FALSE

	[msave .quick_slot] = -1

	M_SetupNextMenu MainMenu

	; Handle various version dependencies, like HELP1/2, and the
	; different number of episodes (four for Ultimate DOOM).
	; The same hacks were used in the original Doom EXEs.

	if gameversion >= exe_ultimate {
		[MainMenuItems 4 .func] = [ref M_ReadThis2]
		[ReadMenu2 .parent] = NULL
	}

	if matches? gameversion exe_final exe_final2 {
		[ReadMenu2 .drawer] = [ref M_DrawReadThisCommercial]
	}

	if gamemode == commercial {
		; remove the "Read This" option in main menu
		I_MemCopy [ref MainMenuItems 4] [ref MainMenuItems 5] menuitem_t.size

		[MainMenu .total] = 5
		[MainMenu .y]     = 72

		[NewMenu .parent] = MainMenu

		[ReadMenu1 .x] = 330  ; position of skull
		[ReadMenu1 .y] = 165  ;
		[ReadMenu1 .drawer]      = [ref M_DrawReadThisCommercial]
		[ReadMenu1Items 0 .func] = [ref M_FinishReadThis]
	}

	; Versions of doom.exe before the Ultimate Doom release only had
	; three episodes; if we're emulating one of those then don't try
	; to show episode four.  If we are, then do show episode four
	; (and crash if missing).

	if gameversion < exe_ultimate {
		[EpiMenu .total] = 3

	} else if gameversion == exe_chex {
		; chex.exe shows only one episode.
		[EpiMenu .total] = 1
	}
}
