;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C) 2005-2006 Andrey Budko
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#private

; output state from P_LineOpening
zero-var opentop    fixed_t
zero-var openbottom fixed_t
zero-var openrange  fixed_t
zero-var lowfloor   fixed_t

; flags for P_PathTraverse
const PT_ADDLINES  = 1
const PT_ADDTHINGS = 2
const PT_EARLYOUT  = 4

type intercept_obj_u = union {
	.thing  ^mobj_t
	.line   ^line_t
}

type intercept_t = struct {
	.frac     fixed_t  ; fraction along trace line
	.is_line  u32
	.obj      intercept_obj_u
}

const MAXINTERCEPTS = 128

zero-var intercepts  [MAXINTERCEPTS]intercept_t
zero-var num_intercept  s32

zero-var trace     divline_t
zero-var earlyout  bool

const BLOCKSIZE     = 128
const MAPBLOCKSHIFT = FRACBITS + 7

type traverse_func_t = fun (incpt ^intercept_t -> bool)

type visit_line_func_t  = fun (line ^line_t -> bool)
type visit_thing_func_t = fun (mo   ^mobj_t -> bool)


#public

;
; P_PointOnLineSide
; Returns 0 or 1.
;
fun P_PointOnLineSide (x fixed_t, y fixed_t, line ^line_t -> s32) {
	let v1 = [line .v1]

	if [line .dx] == 0 {
		if x <= [v1 .x] {
			return s32 ([line .dy] > 0)
		} else {
			return s32 ([line .dy] < 0)
		}
	}

	if [line .dy] == 0 {
		if y <= [v1 .y] {
			return s32 ([line .dx] < 0)
		} else {
			return s32 ([line .dx] > 0)
		}
	}

	let dx = x - [v1 .x]
	let dy = y - [v1 .y]

	let left  = FixedMul dx ([line .dy] >> FRACBITS)
	let right = FixedMul dy ([line .dx] >> FRACBITS)

	return s32 (left <= right)
}

;
; P_PointOnDivlineSide
; Returns 0 (front) or 1 (back).
;
fun P_PointOnDivlineSide (x fixed_t, y fixed_t, line ^divline_t -> s32) {
	if [line .dx] == 0 {
		if x <= [line .x] {
			return s32 ([line .dy] > 0)
		} else {
			return s32 ([line .dy] < 0)
		}
	}

	if [line .dy] == 0 {
		if y <= [line .y] {
			return s32 ([line .dx] < 0)
		} else {
			return s32 ([line .dx] > 0)
		}
	}

	let dx = x - [line .x]
	let dy = y - [line .y]

	; try to quickly decide by looking at sign bits
	let combo = dx ~ dy ~ [line .dx] ~ [line .dy]

	if combo & 0x80000000 != 0 {
		combo = dx ~ [line .dy]
		return s32 some? (combo & 0x80000000)
	}

	dx = dx >> 8
	dy = dy >> 8

	let left  = FixedMul dx ([line .dy] >> 8)
	let right = FixedMul dy ([line .dx] >> 8)

	return s32 (left <= right)
}

#private

;
; P_BoxOnLineSide
;
; Considers the line to be infinite.
; Returns side 0 or 1, -1 if box crosses the line.
;
fun P_BoxOnLineSide (tmbox ^[4]fixed_t, ld ^line_t -> s32) {
	let v1        = [ld .v1]
	let slopetype = [ld .slopetype]

	let p1 = s32 0
	let p2 = s32 0

	; check two corners of the box against the line

	if slopetype == ST_HORIZONTAL {
		p1 = s32 ([tmbox BOXTOP]    > [v1 .y])
		p2 = s32 ([tmbox BOXBOTTOM] > [v1 .y])

		if [ld .dx] < 0 {
			p1 = p1 ~ 1
			p2 = p2 ~ 1
		}

	} else if slopetype == ST_VERTICAL {
		p1 = s32 ([tmbox BOXRIGHT] < [v1 .x])
		p2 = s32 ([tmbox BOXLEFT]  < [v1 .x])

		if [ld .dy] < 0 {
			p1 = p1 ~ 1
			p2 = p2 ~ 1
		}

	} else if slopetype == ST_POSITIVE {
		p1 = P_PointOnLineSide [tmbox BOXLEFT]  [tmbox BOXTOP]    ld
		p2 = P_PointOnLineSide [tmbox BOXRIGHT] [tmbox BOXBOTTOM] ld

	} else if slopetype == ST_NEGATIVE {
		p1 = P_PointOnLineSide [tmbox BOXRIGHT] [tmbox BOXTOP]    ld
		p2 = P_PointOnLineSide [tmbox BOXLEFT]  [tmbox BOXBOTTOM] ld
	}

	; are both corners on the same side?
	if p1 == p2 {
		return p1
	}

	return -1
}

;
; P_MakeDivline
;
fun P_MakeDivline (line ^line_t, dl ^divline_t) {
	[dl .x]  = [[line .v1] .x]
	[dl .y]  = [[line .v1] .y]

	[dl .dx] = [line .dx]
	[dl .dy] = [line .dy]
}

;
; P_InterceptVector
;
; Returns fractional intercept point along the first divline.
; This is only called by the addthings and addlines traversers.
;
fun P_InterceptVector (v2 ^divline_t, v1 ^divline_t -> fixed_t) {
	let p1 = FixedMul ([v1 .dy] >> 8) [v2 .dx]
	let p2 = FixedMul ([v1 .dx] >> 8) [v2 .dy]

	let den = p1 - p2

	if den == 0 {
		; parallel lines
		return 0
	}

	let dx = [v1 .x] - [v2 .x]
	let dy = [v2 .y] - [v1 .y]

	p1 = FixedMul (dx >> 8) [v1 .dy]
	p2 = FixedMul (dy >> 8) [v1 .dx]

	let num = p1 + p2

	let frac = FixedDiv num den
	return frac
}

;
; P_LineOpening
;
; Sets opentop and openbottom to the window
; through a two sided line.
;
fun P_LineOpening (ld ^line_t) {
	; single sided line?
	if [ld .sidenum 1] == -1 {
		openrange = 0
		return
	}

	let front = [ld .front]
	let back  = [ld .back]

	opentop    = min [front .ceilh]  [back .ceilh]
	openbottom = max [front .floorh] [back .floorh]
	lowfloor   = min [front .floorh] [back .floorh]

	openrange = opentop - openbottom
}


;;
;; THING POSITION SETTING
;;

inline-fun Block_X (x fixed_t -> s32) {
	x = x - bmaporgx
	x = x >> MAPBLOCKSHIFT
	return x
}

inline-fun Block_Y (y fixed_t -> s32) {
	y = y - bmaporgy
	y = y >> MAPBLOCKSHIFT
	return y
}

fun Block_Valid (bx s32, by s32 -> bool) {
	let check_x = bx >= 0 and bx < bmapwidth
	let check_y = by >= 0 and by < bmapheight

	return check_x and check_y
}

;
; P_UnsetThingPosition
;
; Unlinks a thing from block map and sectors.
; On each position change, BLOCKMAP and other
; lookups maintaining lists ot things inside
; these structures need to be updated.
;
fun P_UnsetThingPosition (mo ^mobj_t) {
	; unlink from sector

	if [mo .flags] & MF_NOSECTOR == 0 {
		if ref? [mo .snext] {
			[[mo .snext] .sprev] = [mo .sprev]
		}

		if ref? [mo .sprev] {
			[[mo .sprev] .snext] = [mo .snext]
		} else {
			let sec = [[mo .subsector] .sector]
			[sec .thinglist] = [mo .snext]
		}
	}

	; unlink from block map

	if [mo .flags] & MF_NOBLOCKMAP == 0 {
		if ref? [mo .bnext] {
			[[mo .bnext] .bprev] = [mo .bprev]
		}

		if ref? [mo .bprev] {
			[[mo .bprev] .bnext] = [mo .bnext]
		} else {
			let bx = Block_X [mo .x]
			let by = Block_Y [mo .y]

			if Block_Valid bx by {
				let ofs = by * bmapwidth
				ofs = ofs + bx
				[blocklinks ofs] = [mo .bnext]
			}
		}
	}
}

;
; P_SetThingPosition
;
; Links a thing into both a block and a subsector
; based on it's x y.
; Sets thing->subsector properly
;
fun P_SetThingPosition (mo ^mobj_t) {
	; link into sector

	let sub = R_PointInSubsector [mo .x] [mo .y]
	[mo .subsector] = sub

	if [mo .flags] & MF_NOSECTOR == 0 {
		let sec = [sub .sector]

		[mo .sprev] = NULL
		[mo .snext] = [sec .thinglist]

		if ref? [sec .thinglist] {
			[[sec .thinglist] .sprev] = mo
		}

		[sec .thinglist] = mo
	}

	; link into blockmap

	if [mo .flags] & MF_NOBLOCKMAP == 0 {
		let bx = Block_X [mo .x]
		let by = Block_Y [mo .y]

		if Block_Valid bx by {
			let ofs = by * bmapwidth
			ofs = ofs + bx

			let link = [ref blocklinks ofs]

			[mo .bprev] = NULL
			[mo .bnext] = [link]

			if ref? [link] {
				[[link] .bprev] = mo
			}

			[link] = mo
		} else {
			; thing is off the map
			[mo .bnext] = NULL
			[mo .bprev] = NULL
		}
	}
}


;;
;; BLOCK MAP ITERATORS
;;


;
; P_BlockLinesIterator
;
; For each line/thing in the given mapblock,
; call the passed PIT_* function.
; If the function returns false,
; exit with false without checking anything else.
;
; The validcount flags are used to avoid checking lines
; that are marked in multiple mapblocks,
; so increment validcount before the first call to
; P_BlockLinesIterator, then make one or more calls to it.
;
fun P_BlockLinesIterator (bx s32, by s32, func ^visit_line_func_t -> bool) {
	let end_marker = u16 -1

	if Block_Valid bx by {
		let ofs = by * bmapwidth + bx

		; read offset into BLOCKMAP lump
		ofs = s32 [blockmapofs ofs]

		let list = [ref blockmaplump ofs]

		loop until [list] == end_marker {
			let ld = [ref lines [list]]

			; line has already been checked?
			if [ld .validcount] != validcount {
				[ld .validcount] = validcount

				if not call func ld {
					return FALSE
				}
			}

			list = list + 2
		}
	}

	return TRUE
}

;
; P_BlockThingsIterator
;
fun P_BlockThingsIterator (bx s32, by s32, func ^visit_thing_func_t -> bool) {
	if Block_Valid bx by {
		let ofs = by * bmapwidth
		ofs = ofs + bx

		let mo = [blocklinks ofs]

		loop until null? mo {
			if not call func mo {
				return FALSE
			}
			mo = [mo .bnext]
		}
	}

	return TRUE
}

;;
;; INTERCEPT ROUTINES
;;

;
; PIT_AddLineIntercepts
;
; Looks for lines in the given block that intercept
; the given trace to add to the intercepts list.
;
; A line is crossed if its endpoints are on opposite sides
; of the trace.  Returns true if earlyout and a solid line hit.
;
fun PIT_AddLineIntercepts (ld ^line_t -> bool) {
	let v1 = [ld .v1]
	let v2 = [ld .v2]

	; avoid precision problems with two routines
	let check_x = (abs [trace .dx]) > (16 * FRACUNIT)
	let check_y = (abs [trace .dy]) > (16 * FRACUNIT)

	let s1 = s32 0
	let s2 = s32 0

	if check_x or check_y {
		s1 = P_PointOnDivlineSide [v1 .x] [v1 .y] trace
		s2 = P_PointOnDivlineSide [v2 .x] [v2 .y] trace
	} else {
		let x1 = [trace .x]
		let y1 = [trace .y]

		let x2 = x1 + [trace .dx]
		let y2 = y1 + [trace .dy]

		s1 = P_PointOnLineSide x1 y1 ld
		s2 = P_PointOnLineSide x2 y2 ld
	}

	if s1 == s2 {
		; the line isn't crossed
		return TRUE
	}

	; hit the line

	let dl = stack-var divline_t
	P_MakeDivline ld dl

	let frac = P_InterceptVector trace dl

	if frac < 0 {
		; behind source
		return TRUE
	}

	; try to early out the check at a solid wall
	if earlyout {
		if frac < FRACUNIT and null? [ld .back] {
			; stop checking
			return FALSE
		}
	}

	if num_intercept < MAXINTERCEPTS {
		let incpt = [ref intercepts num_intercept]
		num_intercept = num_intercept + 1

		[incpt .frac]      = frac
		[incpt .is_line]   = 1
		[incpt .obj .line] = ld
	}

	; continue checking
	return TRUE
}

;
; PIT_AddThingIntercepts
;
fun PIT_AddThingIntercepts (mo ^mobj_t -> bool) {
	let r = [mo .radius]

	; check a corner to corner cross-section for hit
	let combo = [trace .dx] ~ [trace .dy]

	let x1 = [mo .x] - [mo .radius]
	let y1 = [mo .y] - [mo .radius]
	let x2 = [mo .x] + [mo .radius]
	let y2 = [mo .y] + [mo .radius]

	if combo > 0 {
		swap y1 y2
	}

	let s1 = P_PointOnDivlineSide x1 y1 trace
	let s2 = P_PointOnDivlineSide x2 y2 trace

	if s1 == s2 {
		; line isn't crossed
		return TRUE
	}

	let dl = stack-var divline_t

	[dl .x]  = x1
	[dl .y]  = y1
	[dl .dx] = x2 - x1
	[dl .dy] = y2 - y1

	let frac = P_InterceptVector trace dl

	if frac < 0 {
		; behind source
		return TRUE
	}

	if num_intercept < MAXINTERCEPTS {
		let incpt = [ref intercepts num_intercept]
		num_intercept = num_intercept + 1

		[incpt .frac]       = frac
		[incpt .is_line]    = 0
		[incpt .obj .thing] = mo
	}

	; continue checking
	return TRUE
}

;
; P_TraverseIntercepts
;
; Returns true only if the traverser function returns
; true for all objects in the range 0..FRACUNIT
;
fun P_TraverseIntercepts (func ^traverse_func_t -> bool) {
	let maxfrac = fixed_t FRACUNIT

	let count = num_intercept

	loop until count == 0 {
		count = count - 1

		; find the closest intercept
		let frac = fixed_t FRAC_MAX
		let best = ^intercept_t NULL

		let i = s32 0
		loop while i < num_intercept {
			let scan = [ref intercepts i]

			if [scan .frac] < frac {
				frac = [scan .frac]
				best = scan
			}

			i = i + 1
		}

		if frac > maxfrac {
			; checked everything in range
			return TRUE
		}

		let res = call func best
		if not res {
			; don't bother going farther
			return FALSE
		}

		; mark current intercept as dead
		[best .frac] = FRAC_MAX
	}

	; everything was traversed
	return TRUE
}

;
; P_PathTraverse
;
; Traces a line from x1,y1 to x2,y2,
; calling the traverser function for each.
;
; Returns true if the traverser function returns true
; for all objects in the range.
;
fun P_PathTraverse (x1 fixed_t, y1 fixed_t, x2 fixed_t, y2 fixed_t, flags s32, func ^traverse_func_t -> bool) {
	let do_lines  = (flags & PT_ADDLINES  != 0)
	let do_things = (flags & PT_ADDTHINGS != 0)

	; clear intercept list
	num_intercept = 0

	earlyout   = (flags & PT_EARLYOUT != 0)
	validcount = validcount + 1

	; don't start exactly on a vertical line
	let v = x1 - bmaporgx
	v = v & (BLOCKSIZE * FRACUNIT - 1)
	if v == 0 {
		x1 = x1 + FRACUNIT
	}

	; don't start exactly on a horizontal line
	v = y1 - bmaporgy
	v = v & (BLOCKSIZE * FRACUNIT - 1)
	if v == 0 {
		y1 = y1 + FRACUNIT
	}

	[trace .x]  = x1
	[trace .y]  = y1
	[trace .dx] = x2 - x1
	[trace .dy] = y2 - y1

	x1 = x1 - bmaporgx
	y1 = y1 - bmaporgy
	x2 = x2 - bmaporgx
	y2 = y2 - bmaporgy

	let bx1 = x1 >> MAPBLOCKSHIFT
	let by1 = y1 >> MAPBLOCKSHIFT
	let bx2 = x2 >> MAPBLOCKSHIFT
	let by2 = y2 >> MAPBLOCKSHIFT

	let bx_step = s32 0
	let by_step = s32 0

	let xpartial = (y1 >> 7) & (FRACUNIT - 1)
	let ypartial = (x1 >> 7) & (FRACUNIT - 1)

	if bx1 < bx2 {
		bx_step  = 1
		ypartial = FRACUNIT - ypartial
	} else if bx1 > bx2 {
		bx_step  = -1
	} else {
		ypartial = FRACUNIT
	}

	if by1 < by2 {
		by_step  = 1
		xpartial = FRACUNIT - xpartial
	} else if by1 > by2 {
		by_step  = -1
	} else {
		xpartial = FRACUNIT
	}

	let xstep = fixed_t (256 * FRACUNIT)
	let ystep = fixed_t (256 * FRACUNIT)

	if bx1 != bx2 {
		ystep = FixedDiv (y2 - y1) (abs (x2 - x1))
	}

	if by1 != by2 {
		xstep = FixedDiv (x2 - x1) (abs (y2 - y1))
	}

	let xintercept = (x1 >> 7) + FixedMul xstep xpartial
	let yintercept = (y1 >> 7) + FixedMul ystep ypartial

	; Step through map blocks.
	; Count is there to prevent a round-off error from skipping the break.
	let bx = bx1
	let by = by1

	let count = s32 64
	loop until count == 0 {
		count = count - 1

		if do_lines {
			let res = P_BlockLinesIterator bx by PIT_AddLineIntercepts
			if not res {
				return FALSE
			}
		}

		if do_things {
			let res = P_BlockThingsIterator bx by PIT_AddThingIntercepts
			if not res {
				return FALSE
			}
		}

		let last_block = bx == bx2 and by == by2
		break if last_block

		if yintercept >> FRACBITS == by {
			bx = bx + bx_step
			yintercept = yintercept + ystep

		} else if xintercept >> FRACBITS == bx {
			by = by + by_step
			xintercept = xintercept + xstep
		}
	}

	; go through the sorted list
	return P_TraverseIntercepts func
}
