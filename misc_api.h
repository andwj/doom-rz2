//
// Copyright(C) 1993-1996 Id Software, Inc.
// Copyright(C) 2005-2014 Simon Howard
// Copyright(C)      2021 Andrew Apted
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

#ifndef __MISC_API__
#define __MISC_API__

#include <stdio.h>
#include <stdarg.h>

//
//  m_argv
//

void M_InitArgs(int argc, char **argv);

const char *M_GetArg (int p);

// Returns the position of the given parameter
// in the arg list (0 if not found).
int M_CheckParm (char *check);

// Same as M_CheckParm, but checks that num_args arguments are available
// following the specified argument.
int M_CheckParmWithArgs(char *check, int num_args);

// Parameter has been specified?
boolean M_ParmExists(char *check);

void M_FindResponseFile(void);


//
//  m_config
//

extern char *configdir;

void M_LoadDefaults(void);
void M_SaveDefaults(void);
void M_SetConfigDir(char *dir);
void M_BindIntVariable(char *name, int *variable);
void M_BindStringVariable(char *name, char **variable);
boolean M_SetVariable(char *name, char *value);
int M_GetIntVariable(char *name);
const char *M_GetStringVariable(char *name);


//
//  m_misc
//

boolean M_WriteFile(char *name, void *source, int length);
char *M_FileCaseExists(char *file);
long M_FileLength(FILE *handle);
void M_ExtractFileBase(char *path, char *dest);

int M_Atoi(const char *str);
void M_ForceUppercase(char *text);
void M_ForceLowercase(char *text);
char *M_StringDuplicate(const char *orig);
boolean M_StringCopy(char *dest, const char *src, unsigned int dest_size);
boolean M_StringConcat(char *dest, const char *src, unsigned int dest_size);
char *M_StringJoin(const char *A, const char *B);
char *M_StringJoin3(const char *A, const char *B, const char *C);
boolean M_StringStartsWith(const char *s, const char *prefix);
boolean M_StringEndsWith(const char *s, const char *suffix);
int M_StrCmp(const char *A, const char *B);
int M_StrCaseCmp(const char *A, const char *B);
int M_StrNCmp(const char *A, const char *B, unsigned int n);
int M_StrNCaseCmp(const char *A, const char *B, unsigned int n);
unsigned int M_StrLen(const char *A);
void M_FormatNum(char *buf, unsigned int buf_len, const char *s, int arg);
char M_ToLower(char ch);
char M_ToUpper(char ch);
unsigned int M_HashName8(char *str, unsigned int total);


//
//  memio
//

typedef struct _MEMFILE MEMFILE;

typedef enum
{
	MEM_SEEK_SET,
	MEM_SEEK_CUR,
	MEM_SEEK_END,
} mem_rel_t;

MEMFILE * mem_fopen_read(void *buf, unsigned int buflen);
MEMFILE * mem_fopen_write(void);
void mem_freopen_read(MEMFILE *stream);
void mem_fclose(MEMFILE *stream);

unsigned int mem_fread(void *buf, unsigned int size, unsigned int nmemb, MEMFILE *stream);
unsigned int mem_fwrite(const void *ptr, unsigned int size, unsigned int nmemb, MEMFILE *stream);

int mem_ftell(MEMFILE *stream);
int mem_fseek(MEMFILE *stream, int offset, mem_rel_t whence);

#endif /* __MISC_API__ */
