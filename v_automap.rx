;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

zero-var automapactive bool
zero-var am_cheat      s32


; these are in engine module

#private

; palette colors
const BLACK    = 0
const WHITE    = 209
const REDS     = 176
const GREENS   = 112
const GRAYS    = 96
const BROWNS   = 64
const YELLOWS  = 231

; Automap colors
const BACKGROUND   = BLACK
const THINGCOLORS  = GREENS
const WALLCOLORS   = REDS
const TSWALLCOLORS = GRAYS
const FDWALLCOLORS = BROWNS
const CDWALLCOLORS = YELLOWS
const GRIDCOLORS   = GRAYS + 8
const XHAIRCOLORS  = GRAYS
const INVISCOLOR   = 246  ; *close* to black
const ALLMAPCOLOR  = GRAYS + 3

; --- drawing stuff ---

const AM_NUMMARKPOINTS = 10

; scale on entry
const INITSCALEMTOF = FRACUNIT / 5

; how much the automap moves window per tic in frame-buffer coordinates
; moves 140 pixels in 1 second
const F_PANINC = 4

; how much zoom-in per tic
; goes to 2x in 1 second
const M_ZOOMIN  = 66847  ; FRACUNIT * 1.02

; how much zoom-out per tic
; pulls out to 0.5x in 1 second
const M_ZOOMOUT = 64250  ; FRACUNIT / 1.02

type fpoint_t = struct {
	.x  s32
	.y  s32
}

type fline_t = struct {
	.a  fpoint_t
	.b  fpoint_t
}

type mpoint_t = struct {
	.x  fixed_t
	.y  fixed_t
}

type mline_t = struct {
	.a  mpoint_t
	.b  mpoint_t
}

;
; The vector graphics for the automap.
; A line drawing of the player pointing right, starting from the middle.
; Also a triangle used for monsters (etc).
;
const PLAYER_RADIUS  = 16 * FRACUNIT
const MONSTER_RADIUS = 16 * FRACUNIT

const +P1 =   FRACUNIT
const -P1 = - FRACUNIT
const +P2 = +P1 - (+P1 / 2)
const +P4 = +P1 / 4
const -P4 = -P1 / 4
const -P3 = -P1 + (+P1 * 3 / 8)
const -P8 = -P1 + (+P1 / 8)
const -P9 = -P1 - (+P1 / 8)

rom-var player_arrow [7]mline_t = {
	{ { -P8 0 }  { +P1  0  } }  ; -----
	{ { +P1 0 }  { +P2 +P4 } }  ; ----->
	{ { +P1 0 }  { +P2 -P4 } }
	{ { -P8 0 }  { -P9 +P4 } }  ; >---->
	{ { -P8 0 }  { -P9 -P4 } }
	{ { -P3 0 }  { -P8 +P4 } }  ; >>--->
	{ { -P3 0 }  { -P8 -P4 } }
}

const +T1 = FRACUNIT
const +T7 = (+T1 * 7) / 10
const -T2 = - (+T1 / 2)
const -T7 = - +T7

rom-var triangle_guy [3]mline_t = {
	{ { +T1  0  }  { -T2 +T7 } }
	{ { -T2 +T7 }  { -T2 -T7 } }
	{ { -T2 -T7 }  { +T1  0  } }
}

var drawgrid     bool = FALSE
var followplayer bool = TRUE

; size of window on screen
zero-var f_w s32
zero-var f_h s32

zero-var m_paninc   mpoint_t  ; how far the window pans each tic (map coords)
zero-var m_zoommul  fixed_t   ; how far the window zooms in each tic (map coords)

;
; location and size of window on map (map coords)
;
zero-var m_x fixed_t   ; lower-left corner
zero-var m_y fixed_t   ;

zero-var m_x2 fixed_t  ; upper-right corner
zero-var m_y2 fixed_t  ;

zero-var m_w fixed_t   ; m_x2 - m_x
zero-var m_h fixed_t   ; m_y2 - m_y

; state for big/normal toggle
zero-var bigstate bool

zero-var old_m_x fixed_t
zero-var old_m_y fixed_t
zero-var old_m_w fixed_t
zero-var old_m_h fixed_t

; bounding box of level geometry
zero-var lev_min_x fixed_t
zero-var lev_min_y fixed_t
zero-var lev_max_x fixed_t
zero-var lev_max_y fixed_t

zero-var min_scale_mtof fixed_t  ; used to tell when to stop zooming out
zero-var max_scale_mtof fixed_t  ; used to tell when to stop zooming in

; used by MTOF to scale from map-to-frame-buffer coords
var scale_mtof fixed_t = INITSCALEMTOF
; used by FTOM to scale from frame-buffer-to-map coords (=1/scale_mtof)
var scale_ftom fixed_t = 0

const MARK_EMPTY = -77777

zero-var markpoints [AM_NUMMARKPOINTS]mpoint_t  ; where the points are
zero-var markpointnext s32
zero-var markpointmsg  [32]uchar

const AMSTR_FOLLOWON     = "Follow Mode ON"
const AMSTR_FOLLOWOFF    = "Follow Mode OFF"
const AMSTR_GRIDON       = "Grid ON"
const AMSTR_GRIDOFF      = "Grid OFF"
const AMSTR_MARKEDSPOT   = "Marked Spot %d"
const AMSTR_MARKSCLEARED = "All Marks Cleared"

const HU_TITLEX = 0
const HU_TITLEY = 167  ; baseline

rom-var player_colors [4]u8 = {
	GREENS  GRAYS  BROWNS  REDS
}

; translates between frame-buffer and map distances
inline-fun FTOM (x s32 -> fixed_t) {
	x = x << FRACBITS
	return FixedMul x scale_ftom
}

inline-fun MTOF (x fixed_t -> s32) {
	x = FixedMul x scale_mtof
	return x >> FRACBITS
}

; translates between frame-buffer and map coordinates
inline-fun CXMTOF (x fixed_t -> s32) {
	x = x - m_x
	return MTOF x
}

inline-fun CYMTOF (y fixed_t -> s32) {
	y = y - m_y
	return f_h - MTOF y
}

fun AM_activateNewScale () {
	m_x = m_x + m_w / 2
	m_y = m_y + m_h / 2

	m_w = FTOM f_w
	m_h = FTOM f_h

	m_x = m_x - m_w / 2
	m_y = m_y - m_h / 2

	m_x2 = m_x + m_w
	m_y2 = m_y + m_h
}

fun AM_saveScaleAndLoc () {
	old_m_x = m_x
	old_m_y = m_y
	old_m_w = m_w
	old_m_h = m_h
}

fun AM_restoreScaleAndLoc () {
	m_w = old_m_w
	m_h = old_m_h

	if followplayer {
		let mo    = [con_pl .mo]
		m_x = [mo .x] - m_w / 2
		m_y = [mo .y] - m_h / 2
	} else {
		m_x = old_m_x
		m_y = old_m_y
	}

	m_x2 = m_x + m_w
	m_y2 = m_y + m_h

	; change the scaling multipliers
	let ff_w = f_w << FRACBITS
	scale_mtof = FixedDiv ff_w m_w
	scale_ftom = FixedDiv FRACUNIT scale_mtof
}

;
; Determines bounding box of all vertices,
; sets global variables controlling zoom range.
;
fun AM_findMinMaxBoundaries () {
	lev_min_x = FRAC_MAX
	lev_min_y = FRAC_MAX
	lev_max_x = FRAC_MIN
	lev_max_y = FRAC_MIN

	let i = s32 0

	loop while i < numvertexes {
		let x = [vertexes i .x]
		let y = [vertexes i .y]

		lev_min_x = min lev_min_x x
		lev_min_y = min lev_min_y y

		lev_max_x = max lev_max_x x
		lev_max_y = max lev_max_y y

		i = i + 1
	}

	let level_w = lev_max_x - lev_min_x
	let level_h = lev_max_y - lev_min_y

	let ff_w = f_w << FRACBITS
	let ff_h = f_h << FRACBITS

	let a = FixedDiv ff_w level_w
	let b = FixedDiv ff_h level_h

	min_scale_mtof = min a b
	max_scale_mtof = FixedDiv ff_h (2 * PLAYER_RADIUS)
}

fun AM_initVariables () {
	; reset panning and zooming state
	[m_paninc .x] = 0
	[m_paninc .y] = 0
	m_zoommul   = FRACUNIT

	m_w = FTOM f_w
	m_h = FTOM f_h

	let mo = [con_pl .mo]

	m_x = [mo .x] - m_w / 2
	m_y = [mo .y] - m_h / 2

	m_x2 = m_x + m_w
	m_y2 = m_y + m_h

	; for saving & restoring
	old_m_x = m_x
	old_m_y = m_y
	old_m_w = m_w
	old_m_h = m_h
}

fun AM_clearMarks () {
	let i = s32 0

	loop while i < AM_NUMMARKPOINTS {
		[markpoints i .x] = MARK_EMPTY
		[markpoints i .y] = MARK_EMPTY

		i = i + 1
	}

	markpointnext = 0
}

;
; adds a marker at the current location
;
fun AM_addMark () {
	let next = markpointnext

	let mo = [con_pl .mo]

	[markpoints next .x] = [mo .x]
	[markpoints next .y] = [mo .y]

	; create message for the player
	M_FormatNum      markpointmsg 32 AMSTR_MARKEDSPOT next
	D_ConsoleMessage markpointmsg

	next = next + 1
	next = next % AM_NUMMARKPOINTS

	markpointnext = next
}

;
; set the window scale to the maximum size
;
fun AM_minOutWindowScale () {
	scale_mtof = min_scale_mtof
	scale_ftom = FixedDiv FRACUNIT scale_mtof

	AM_activateNewScale
}

;
; set the window scale to the minimum size
;
fun AM_maxOutWindowScale () {
	scale_mtof = max_scale_mtof
	scale_ftom = FixedDiv FRACUNIT scale_mtof

	AM_activateNewScale
}

;
; Panning
;
fun AM_panWindow () {
	; keep middle of window within bounds of map
	let x1 = lev_min_x - m_w / 2
	let x2 = lev_max_x - m_w / 2

	m_x = m_x + [m_paninc .x]
	m_x = max m_x x1
	m_x = min m_x x2

	let y1 = lev_min_y - m_h / 2
	let y2 = lev_max_y - m_h / 2

	m_y = m_y + [m_paninc .y]
	m_y = max m_y y1
	m_y = min m_y y2

	m_x2 = m_x + m_w
	m_y2 = m_y + m_h
}

;
; Zooming
;
fun AM_zoomWindow () {
	scale_mtof = FixedMul scale_mtof m_zoommul
	scale_ftom = FixedDiv FRACUNIT scale_mtof

	if scale_mtof < min_scale_mtof {
		AM_minOutWindowScale
	} else if scale_mtof > max_scale_mtof {
		AM_maxOutWindowScale
	} else {
		AM_activateNewScale
	}
}

fun AM_followPlayer () {
	let mo = [con_pl .mo]

	let fx = MTOF [mo .x]
	let fy = MTOF [mo .y]

	m_x  = FTOM fx - m_w / 2
	m_y  = FTOM fy - m_h / 2

	m_x2 = m_x + m_w
	m_y2 = m_y + m_h
}

fun AM_toggleBigState () {
	bigstate = not bigstate

	if bigstate {
		AM_saveScaleAndLoc
		AM_minOutWindowScale
	} else {
		AM_restoreScaleAndLoc
	}
}

fun AM_toggleFollowPlayer () {
	followplayer = not followplayer

	if followplayer {
		D_ConsoleMessage AMSTR_FOLLOWON
	} else {
		D_ConsoleMessage AMSTR_FOLLOWOFF
	}
}

fun AM_toggleGrid () {
	drawgrid = not drawgrid

	if drawgrid {
		D_ConsoleMessage AMSTR_GRIDON
	} else {
		D_ConsoleMessage AMSTR_GRIDOFF
	}
}

;----------------------------------------------------------------------

;
; Clear automap frame buffer.
;
fun AM_clearFB () {
	let fb   = videoBuffer
	let size = f_w * f_h

	I_MemSet fb BACKGROUND (u32 size)
}

fun AM_drawTitle () {
	; determine height of font
	let char0  = [hu_font 0]
	let font_h = little-endian [char0 .height]

	let x = s32 HU_TITLEX
	let y = s32 HU_TITLEY - font_h

	HU_DrawText x y hu_map_name
}

;
; Automap clipping of lines.
;
; Based on Cohen-Sutherland clipping algorithm but with a slightly
; faster reject and precalculated slopes.
;
const O_LEFT   = 1
const O_RIGHT  = 2
const O_BOTTOM = 4
const O_TOP    = 8

fun AM_clipMline (ml ^mline_t, fl ^fline_t -> bool) {
	; do trivial rejects
	let code1 = OUTCODE_MAP [ml .a .x] [ml .a .y]
	let code2 = OUTCODE_MAP [ml .b .x] [ml .b .y]

	if code1 & code2 != 0 {
		return FALSE
	}

	; transform to frame-buffer coordinates.
	[fl .a .x] = CXMTOF [ml .a .x]
	[fl .a .y] = CYMTOF [ml .a .y]
	[fl .b .x] = CXMTOF [ml .b .x]
	[fl .b .y] = CYMTOF [ml .b .y]

	code1 = OUTCODE [fl .a .x] [fl .a .y]
	code2 = OUTCODE [fl .b .x] [fl .b .y]

	loop {
		if code1 | code2 == 0 {
			return TRUE
		}

		; trivially outside?
		if code1 & code2 != 0 {
			return FALSE
		}

		; may be partially inside box...

		; find an outside point
		let outside = max code1 code2

		let dx = [fl .b .x] - [fl .a .x]
		let dy = [fl .a .y] - [fl .b .y]

		let new_x = s32 0
		let new_y = s32 0

		; clip to a single side
		if outside & O_TOP != 0 {
			dx = dx * [fl .a .y]
			dx = dx / dy

			new_x = [fl .a .x] + dx

		} else if outside & O_BOTTOM != 0 {
			let fy = [fl .a .y] - f_h
			dx = dx * fy
			dx = dx / dy

			new_x = [fl .a .x] + dx
			new_y = f_h - 1

		} else if outside & O_LEFT != 0 {
			dy = dy * [fl .a .x]
			dy = dy / dx

			new_y = [fl .a .y] + dy

		} else if outside & O_RIGHT != 0 {
			let fx = [fl .a .x] - f_w
			fx = fx + 1
			dy = dy * fx
			dy = dy / dx

			new_x = f_w - 1
			new_y = [fl .a .y] + dy
		}

		if outside == code1 {
			[fl .a .x] = new_x
			[fl .a .y] = new_y

			code1 = OUTCODE new_x new_y
		} else {
			[fl .b .x] = new_x
			[fl .b .y] = new_y

			code2 = OUTCODE new_x new_y
		}
	}
}

inline-fun OUTCODE (fx s32, fy s32 -> u8) {
	let code = u8 0

	if fx < 0 {
		code = code | O_LEFT
	} else if fx >= f_w {
		code = code | O_RIGHT
	}

	if fy < 0 {
		code = code | O_TOP
	} else if fy >= f_h {
		code = code | O_BOTTOM
	}

	return code
}

inline-fun OUTCODE_MAP (mx fixed_t, my fixed_t -> u8) {
	let code = u8 0

	if mx <= m_x {
		code = code | O_LEFT
	} else if mx >= m_x2 {
		code = code | O_RIGHT
	}

	if my <= m_y {
		code = code | O_BOTTOM
	} else if my >= m_y2 {
		code = code | O_TOP
	}

	return code
}

;
; Classic Bresenham w/ whatever optimizations needed for speed.
;
fun AM_drawFline (fl ^fline_t, color u8) {
	let fb = videoBuffer

	let dx = [fl .b .x] - [fl .a .x]
	let dy = [fl .b .y] - [fl .a .y]

	let ax = abs dx
	let ay = abs dy

	let sx = s32 (dx >= 0) * 2 - 1
	let sy = s32 (dy >= 0) * 2 - 1

	; current point
	let x = [fl .a .x]
	let y = [fl .a .y]

	if ax > ay {
		let d = ay - ax

		loop {
			PUTDOT fb x y color

			break if x == [fl .b .x]

			if d >= 0 {
				y = y + sy
				d = d - ax - ax
			}

			x = x + sx
			d = d + ay + ay
		}

	} else {
		let d = ax - ay

		loop {
			PUTDOT fb x y color

			break if y == [fl .b .y]

			if d >= 0 {
				x = x + sx
				d = d - ay - ay
			}

			y = y + sy
			d = d + ax + ax
		}
	}
}

inline-fun PUTDOT (fb ^[0]pixel_t, x s32, y s32, color u8) {
	let offset  = y * SCREENWIDTH + x
	[fb offset] = color
}

;
; Clip lines, draw visible part sof lines.
;
fun AM_drawMline (ml ^mline_t, color u8) {
	let fl = stack-var fline_t

	if AM_clipMline ml fl {
		AM_drawFline fl color
	}
}

;
; Draws blockmap aligned grid lines.
;
fun AM_drawGrid () {
	if not drawgrid {
		return
	}

	let ml = stack-var mline_t

	; figure out start of vertical gridlines
	let x = m_x / (128 * FRACUNIT)
	x = x     * (128 * FRACUNIT)

	; draw vertical gridlines
	loop while x < m_x2 {
		[ml .a .x] = x
		[ml .a .y] = m_y

		[ml .b .x] = x
		[ml .b .y] = m_y2

		AM_drawMline ml GRIDCOLORS

		x = x + (128 * FRACUNIT)
	}

	; figure out start of horizontal gridlines
	let y = m_y / (128 * FRACUNIT)
	y = y     * (128 * FRACUNIT)

	; draw horizontal gridlines
	loop while y < m_y2 {
		[ml .a .x] = m_x
		[ml .a .y] = y

		[ml .b .x] = m_x2
		[ml .b .y] = y

		AM_drawMline ml GRIDCOLORS

		y = y + (128 * FRACUNIT)
	}
}

;
; Determines visible lines, draws them.
; This is LineDef based, not LineSeg based.
;
fun AM_drawWalls () {
	let i = s32 0
	loop while i < numlines {
		let ld    = [ref lines i]
		let color = AM_colorForWall ld

		if color != BLACK {
			let ml = stack-var mline_t

			[ml .a .x] = [[ld .v1] .x]
			[ml .a .y] = [[ld .v1] .y]
			[ml .b .x] = [[ld .v2] .x]
			[ml .b .y] = [[ld .v2] .y]

			AM_drawMline ml color
		}

		i = i + 1
	}
}

fun AM_colorForWall (ld ^line_t -> u8) {
	let flags = [ld .flags]

	let mapped = (flags & ML_MAPPED   != 0)
	let secret = (flags & ML_SECRET   != 0)
	let nodraw = (flags & ML_DONTDRAW != 0)

	if am_cheat > 0 {
		mapped = TRUE
		nodraw = FALSE
	}

	if not mapped {
		; show unseen lines when player has the AllMap powerup
		if [con_pl .powers pw_allmap] != 0 {
			return ALLMAPCOLOR
		}

		return BLACK
	}

	if nodraw {
		return BLACK
	}

	; one-sided line?
	if null? [ld .back] {
		return WALLCOLORS
	}

	; secret door?
	if secret {
		return WALLCOLORS
	}

	let back  = [ld .back]
	let front = [ld .front]

	; floor level change?
	if [back .floorh] != [front .floorh] {
		return FDWALLCOLORS
	}

	; ceiling level change?
	if [back .ceilh] != [front .ceilh] {
		return CDWALLCOLORS
	}

	if am_cheat > 0 {
		return TSWALLCOLORS
	}

	return BLACK
}

;
; Rotation in 2D.
; Used to rotate player arrow line character.
;
fun AM_rotate (pt ^mpoint_t, ang angle_t) {
	ang = ang >> ANGLEFINESHIFT
	let cos = [finecosine ang]
	let sin = [finesine   ang]

	let xc = FixedMul [pt .x] cos
	let xs = FixedMul [pt .x] sin

	let yc = FixedMul [pt .y] cos
	let ys = FixedMul [pt .y] sin

	[pt .x] = xc - ys
	[pt .y] = xs + yc
}

fun AM_drawLineCharacter (guy ^[0]mline_t, count s32, scale fixed_t, ang angle_t, color u8, x fixed_t, y fixed_t) {
	let ml = stack-var mline_t

	loop while count > 0 {
		count = count - 1
		let src   = [ref guy count]

		; scale
		[ml .a .x] = FixedMul [src .a .x] scale
		[ml .a .y] = FixedMul [src .a .y] scale
		[ml .b .x] = FixedMul [src .b .x] scale
		[ml .b .y] = FixedMul [src .b .y] scale

		; rotate
		AM_rotate [ref ml .a] ang
		AM_rotate [ref ml .b] ang

		; translate
		[ml .a .x] = [ml .a .x] + x
		[ml .a .y] = [ml .a .y] + y
		[ml .b .x] = [ml .b .x] + x
		[ml .b .y] = [ml .b .y] + y

		AM_drawMline ml color
	}
}

fun AM_drawPlayers () {
	if not netgame {
		let mo = [con_pl .mo]
		AM_drawLineCharacter player_arrow 7 PLAYER_RADIUS [mo .angle] WHITE [mo .x] [mo .y]
		return
	}

	let i = s32 0
	loop while i < MAXPLAYERS {
		let p  = [ref players i]
		let mo = [p .mo]

		if [p .in_game] {
			;;  if ( (deathmatch && !singledemo) && p != con_pl)
			;;    continue

			let col_idx = [p .index] % 4
			let color   = [player_colors col_idx]

			if [p .powers pw_invisibility] > 0 {
				color = INVISCOLOR
			}

			AM_drawLineCharacter player_arrow 7 PLAYER_RADIUS [mo .angle] color [mo .x] [mo .y]
		}

		i = i + 1
	}
}

fun AM_drawThings () {
	if am_cheat != 2 {
		return
	}

	let i = s32 0
	loop while i < numsectors {
		let sec = [ref sectors i]

		let mo = [sec .thinglist]
		loop until null? mo {
			; andrewj: skip players, they are shown by arrows
			if null? [mo .player] {
				AM_drawLineCharacter triangle_guy 3 MONSTER_RADIUS [mo .angle] THINGCOLORS [mo .x] [mo .y]
			}

			mo = [mo .snext]
		}

		i = i + 1
	}
}

fun AM_drawMarks () {
	let i = s32 0

	loop while i < AM_NUMMARKPOINTS {
		let mp = [ref markpoints i]

		if [mp .x] != MARK_EMPTY {
			let namebuf = stack-var [16]uchar
			M_FormatNum namebuf 16 "AMMNUM%d" i

			let p = cast ^patch_t W_CacheLumpName namebuf

			let w = s32 little-endian [p .width]
			let h = s32 little-endian [p .height]

			; just in case the patch has weird values
			w = max w 6
			h = max h 6

			; convert to screen coords, clip patch to window
			let fx  = CXMTOF [mp .x]
			let fy  = CYMTOF [mp .y]
			let fx2 = fx + w
			let fy2 = fy + h

			if fx >= 0 and fx2 <= f_w and fy >= 0 and fy2 <= f_h {
				V_DrawPatch fx fy p
			}
		}

		i = i + 1
	}
}

fun AM_drawCrosshair () {
	let offset = f_h + 1
	offset = offset * SCREENWIDTH
	offset = offset / 2

	let fb = videoBuffer

	; single point for now
	[fb offset] = XHAIRCOLORS
}

;----------------------------------------------------------------------

#public

;
; called at the start of every level.
;
fun AM_LevelInit () {
	f_w = SCREENWIDTH
	f_h = SCREENHEIGHT - ST_HEIGHT

	AM_clearMarks
	AM_findMinMaxBoundaries

	; initial scale is the minimum divided by 0.7
	scale_mtof = FixedMul min_scale_mtof 93623
	scale_mtof = min scale_mtof max_scale_mtof

	scale_ftom = FixedDiv FRACUNIT scale_mtof
}

fun AM_Stop  () {
	automapactive = FALSE
}

fun AM_Start () {
	automapactive = TRUE
	viewactive    = FALSE

	AM_initVariables
	AM_panWindow
}

;
; Updates on Game Tick
;
fun AM_Ticker  () {
	if automapactive {
		if followplayer {
			AM_followPlayer
		}

		; change the scale if zooming
		if m_zoommul != FRACUNIT {
			AM_zoomWindow
		}

		; change x,y location if panning
		if [m_paninc .x] | [m_paninc .y] != 0 {
			AM_panWindow
		}
	}
}

;
; Handle events (user inputs) in automap mode.
;
fun AM_Responder (ev ^event_t -> bool) {
	let key = [ev .data1]

	if not automapactive {
		if [ev .type] == ev_keydown {
			if key == key_map_toggle {
				AM_Start
				return TRUE
			}
		}

		return FALSE
	}

	if [ev .type] == ev_keydown {
		if key == key_map_east {  ; pan right
			if not followplayer {
				[m_paninc .x] = FTOM F_PANINC
				return TRUE
			}

		} else if key == key_map_west {  ; pan left
			if not followplayer {
				[m_paninc .x] = FTOM (- F_PANINC)
				return TRUE
			}

		} else if key == key_map_north {  ; pan up
			if not followplayer {
				[m_paninc .y] = FTOM F_PANINC
				return TRUE
			}

		} else if key == key_map_south {  ; pan down
			if not followplayer {
				[m_paninc .y] = FTOM (- F_PANINC)
				return TRUE
			}

		} else if key == key_map_zoomout {  ; zoom out
			m_zoommul = M_ZOOMOUT
			return TRUE

		} else if key == key_map_zoomin {  ; zoom in
			m_zoommul = M_ZOOMIN
			return TRUE

		} else if key == key_map_toggle {
			bigstate   = FALSE
			viewactive = TRUE

			AM_Stop
			return TRUE

		} else if key == key_map_maxzoom {
			AM_toggleBigState
			return TRUE

		} else if key == key_map_follow {
			AM_toggleFollowPlayer
			return TRUE

		} else if key == key_map_grid {
			AM_toggleGrid
			return TRUE

		} else if key == key_map_mark {
			AM_addMark
			return TRUE

		} else if key == key_map_clear {
			AM_clearMarks
			D_ConsoleMessage AMSTR_MARKSCLEARED
			return TRUE
		}

		; ignore an unknown key
		return FALSE
	}

	if [ev .type] == ev_keyup {
		if matches? key key_map_zoomout key_map_zoomin {
			m_zoommul = FRACUNIT

		} else if key == key_map_east {
			[m_paninc .x] = 0

		} else if key == key_map_west {
			[m_paninc .x] = 0

		} else if key == key_map_north {
			[m_paninc .y] = 0

		} else if key == key_map_south {
			[m_paninc .y] = 0
		}

		return FALSE
	}

	return FALSE
}

fun AM_Drawer () {
	if automapactive {
		AM_clearFB

		AM_drawGrid
		AM_drawWalls
		AM_drawPlayers
		AM_drawThings
		AM_drawCrosshair
		AM_drawMarks
		AM_drawTitle
	}
}
