;;
;; Copyright(C) 1993-1996 Id Software, Inc.
;; Copyright(C) 1993-2008 Raven Software
;; Copyright(C) 2005-2014 Simon Howard
;; Copyright(C)      2021 Andrew Apted
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;

#public

; the screen buffer that the v_video.c code draws to.
zero-var dest_screen ^pixel_t

zero-var background_buffer [SCREENPIXELS]pixel_t

fun V_PatchWidth (patch ^patch_t -> s32) {
	let w = little-endian [patch .width]
	return s32 w
}

fun V_PatchHeight (patch ^patch_t -> s32) {
	let h = little-endian [patch .height]
	return s32 h
}

;
; V_DrawPatch
;
; Masks a column based masked pic to the screen.
; andrewj: made it clip to the screen (for F_BunnyScroll).
;
fun V_DrawPatch (x s32, y s32, patch ^patch_t) {
	let w    = little-endian [patch .width]
	let h    = little-endian [patch .height]
	let left = little-endian [patch .left_offset]
	let top  = little-endian [patch .top_offset]

	x = x - (s32 left)
	y = y - (s32 top)

	let col = s16 0

	loop while col < w {
		; clip column to the screen
		jump next_col if x < 0
		break if x >= SCREENWIDTH

		let ofs = little-endian [patch .column_ofs col]
		let column = cast ^post_t (patch + ofs)

		; step through the posts in a column
		loop {
			let delta  = s32 [column .top_delta]
			let length = s32 [column .length]

			break if delta == COLUMN_END

			; clip post to the screen (crudely, all or nothing)
			; WISH: clip properly
			let y1 = y  + delta
			let y2 = y1 + length

			jump next_post if y1 < 0
			jump next_post if y2 > SCREENHEIGHT

			let source = cast ^u8 (column + 3)

			let dest = dest_screen
			dest = dest + (y1 * SCREENWIDTH) + x

			let count = length
			loop until count == 0 {
				[dest] = [source]
				dest   = dest   + SCREENWIDTH
				source = source + 1
				count  = count  - 1
			}

			do next_post {
				column = column + length + 4
			}
		}

		do next_col {
			x   = x   + 1
			col = col + 1
		}
	}
}

;
; V_DrawPatchFlipped
; this is only used for F_CastDrawer, so it does no clipping.
;
fun V_DrawPatchFlipped (x s32, y s32, patch ^patch_t) {
	let w    = little-endian [patch .width]
	let h    = little-endian [patch .height]
	let left = little-endian [patch .left_offset]
	let top  = little-endian [patch .top_offset]

	x = x - (s32 left)
	y = y - (s32 top)

	let col = w - 1

	loop while col >= 0 {
		let ofs = little-endian [patch .column_ofs col]
		let column = cast ^post_t (patch + ofs)

		; step through the posts in a column
		loop {
			let delta  = s32 [column .top_delta]
			let length = s32 [column .length]

			break if delta == COLUMN_END

			let y1 = y + delta

			let source = cast ^u8 (column + 3)

			let dest = dest_screen
			dest = dest + (y1 * SCREENWIDTH) + x

			let count = length
			loop until count == 0 {
				[dest] = [source]
				dest   = dest   + SCREENWIDTH
				source = source + 1
				count  = count  - 1
			}

			column = column + length + 4
		}

		x   = x   + 1
		col = col - 1
	}
}

fun V_DrawPatchName (x s32, y s32, name ^uchar) {
	let patch = cast ^patch_t W_CacheLumpName name
	V_DrawPatch x y patch
}

;
; V_DrawBlock
; Draw a linear block of pixels into the view buffer.
; Does no clipping!
;
fun V_DrawBlock (x s32, y s32, width s32, height s32, src ^pixel_t) {
	y    = y * SCREENWIDTH
	let dest = dest_screen + y + x

	loop while height > 0 {
		I_MemCopy dest src (u32 width)

		src    = src    + width
		dest   = dest   + SCREENWIDTH
		height = height - 1
	}
}

;
; erase the entire screen to a tiled background
;
fun V_FlatBackground (flatname ^uchar) {
	let src  = cast ^[0]u8 W_CacheLumpName flatname
	let dest = dest_screen

	let y = s32 0
	loop while y < SCREENHEIGHT {
		let src2 = src + (y & 63) << 6

		let x = s32 0
		loop while x < SCREENWIDTH {
			let count = SCREENWIDTH - x
			count = min count 64

			I_MemCopy dest src2 (u32 count)

			dest = dest + count
			x    = x    + 64
		}

		y = y + 1
	}
}

;
; V_Init
;
fun V_Init () {
	; no-op!
	; There used to be separate screens that could be drawn to; these are
	; now handled in the upper layers.
}

;
; Set the buffer that the code draws to.
;
fun V_UseBuffer (buffer ^pixel_t) {
	dest_screen = buffer
}

;
; Restore screen buffer to the i_video screen buffer.
;
fun V_RestoreBuffer () {
	dest_screen = videoBuffer
}

;
; V_FillBackScreen
;
; Fills the back screen with a pattern for variable screen sizes.
; Also draws a beveled edge.
;
fun V_FillBackScreen () {
	; If we are running full screen, there is no need to do any of this.
	if viewwidth >= SCREENWIDTH {
		return
	}

	V_UseBuffer background_buffer

	; DOOM I border flat
	let flatname = ^uchar "FLOOR7_2"

	; DOOM II border flat
	if gamemode == commercial {
		flatname = "GRNROCK"
	}

	V_FlatBackground flatname

	let x1 = viewwindowx - 8
	let y1 = viewwindowy - 8

	let x2 = viewwindowx + viewwidth
	let y2 = viewwindowy + viewheight

	; draw the top and bottom parts
	let x = x1 + 8
	loop while x < x2 {
		V_DrawPatchName x y1 "brdr_t"
		V_DrawPatchName x y2 "brdr_b"
		x = x + 8
	}

	; draw the left and right sides
	let y = y1 + 8
	loop while y < y2 {
		V_DrawPatchName x1 y "brdr_l"
		V_DrawPatchName x2 y "brdr_r"
		y = y + 8
	}

	; draw the beveled corners
	V_DrawPatchName x1 y1 "brdr_tl"
	V_DrawPatchName x2 y1 "brdr_tr"
	V_DrawPatchName x1 y2 "brdr_bl"
	V_DrawPatchName x2 y2 "brdr_br"

	V_RestoreBuffer
}

;
; V_VideoErase
; Copies part of background buffer back to main screen.
;
fun V_VideoErase (ofs s32, count s32) {
	if count > 0 {
		let src  = [ref background_buffer] + ofs
		let dest = videoBuffer + ofs

		I_MemCopy dest src (u32 count)
	}
}

;
; V_DrawViewBorder
;
; Draws the border around the view for different size windows.
;
fun V_DrawViewBorder () {
	if viewwidth >= SCREENWIDTH {
		return
	}

	let top = (SCREENHEIGHT - ST_HEIGHT) - viewheight
	top = top / 2

	let left = SCREENWIDTH - viewwidth
	left = left / 2

	; copy top
	let count = top * SCREENWIDTH
	V_VideoErase 0 count

	; copy bottom.  we assume bottom size == top size
	let ofs = top + viewheight
	ofs = ofs * SCREENWIDTH

	V_VideoErase ofs count

	; copy the sides...
	let y   = viewheight
	ofs = count

	loop until y == 0 {
		y = y - 1

		; copy left
		V_VideoErase ofs left
		ofs = ofs + left + viewwidth

		; copy right.  we assume right size == left size
		V_VideoErase ofs left
		ofs = ofs + left
	}
}

;
; V_ScreenShot
;

fun V_ScreenShot () {
	let palette = cast ^[768]u8 W_CacheLumpName "PLAYPAL"

	let filename = M_StringJoin configdir "screenshot.pcx"

	I_WritePCXfile filename videoBuffer SCREENWIDTH SCREENHEIGHT palette

	I_Free filename
}
